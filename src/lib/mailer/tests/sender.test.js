const { sendMail } = require('../sender');

describe('sendMail', function() {
  const from = 'Sender Name <sender@example.com>';
  const message = {
    to: 'Recipient <recipient@example.com>',
    subject: 'Nodemailer is unicode friendly ✔',
    text: 'Hello to myself!',
    html: '<p><b>Hello</b> to myself!</p>',
  };

  it('should return function', () => {
    expect(sendMail()).toEqual(expect.any(Function));
    expect(sendMail({ from })).toEqual(expect.any(Function));
  });

  it('should resolve promise', () => {
    return sendMail()(message).then(response => {
      expect(response).toEqual(undefined);
    });
  });
});
