const { getSMTPEnv } = require('../utils');

describe('getSMTPEnv', function() {
  it('should return structured object', () => {
    const env = {
      SMTP_auth_user: 'user',
      SMTP_keepKeyUpperCase: 'toto',
      SMTP_boolTrue: 'true',
      SMTP_boolFalse: 'false',
      NOSMTP_test: 'test',
    };

    const expected = {
      boolTrue: true,
      boolFalse: false,
      keepKeyUpperCase: 'toto',
      auth: { user: 'user' },
    };
    expect(getSMTPEnv(env)).toEqual(expected);
  });
});
