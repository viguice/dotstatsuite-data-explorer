const { vibe } = require('farso');
const mockHomeEn = require('./mocks/home-en.json');
const mockHomeFr = require('./mocks/home-fr.json');
const mockFreetext = require('./mocks/freetext.json');
const mockFacet = require('./mocks/facet.json');
const mockUsedFilters2 = require('./mocks/used-filters-2.json');
const mockUsedFilters5 = require('./mocks/used-filters-5.json');
const mockResults11 = require('./mocks/results-11.json');
const mockSingleResult = require('./mocks/single-result.json');

const defaultSearchBody = {
  lang: 'en',
  search: '',
  rows: /^\d*$/,
  start: /^\d*$/,
  sort: 'score desc, sname asc, indexationDate desc',
};

const homeSearchBody = {
  lang: 'en',
  rows: 0,
  fl: ['Topic', 'Thème', 'Reference area', 'Frequency', 'datasourceId'],
  facets: { datasourceId: ['ds-demo-release', 'ds-demo-reset'] },
};

const isIndexedBody = {
  lang: 'en',
  rows: 0,
  fl: ['dataflowId'],
  facets: { datasourceId: ['ds-demo-release'] },
};

const isIndexedResponse = isIndexed => ({
  numFound: isIndexed ? 1 : 0,
  start: 0,
  dataflows: [],
  facets: {},
  highlighting: {},
});

vibe.default('oecd', mock => {
  mock('search')
    .checkBody({
      ...defaultSearchBody,
      facets: {
        Topic: ['0|Economy#ECO#'],
        'Reference area': ['0|France#FR#'],
        datasourceId: ['ds-demo-release'],
      },
    })
    .reply((_, res) => res.json(mockResults11));
  mock('search')
    .checkBody({
      ...defaultSearchBody,
      facets: {
        datasourceId: ['ds-demo-reset'],
      },
    })
    .reply((_, res) => res.json(mockSingleResult));
  mock('search')
    .checkBody({
      ...defaultSearchBody,
      facets: {
        Stocks: ['0|Balance of primary incomes#B5G#'],
        'Reference area': ['0|France#FR#'],
        'Reporting institutional sector': ['0|General government#S13#'],
        datasourceId: ['ds-demo-release'],
      },
    })
    .reply((_, res) => res.json(mockUsedFilters2));
  mock('search')
    .checkBody({
      ...defaultSearchBody,
      facets: {
        'Reference area': ['0|France#FR#'],
        'Reporting institutional sector': ['0|General government#S13#'],
        datasourceId: ['ds-demo-release'],
      },
    })
    .reply((_, res) => res.json(mockUsedFilters5));
  mock('search')
    .checkBody({
      ...defaultSearchBody,
      search: 'tourism',
      facets: { datasourceId: ['ds-demo-release'] },
    })
    .reply((_, res) => res.json(mockFreetext));
  mock('search')
    .checkBody({
      ...defaultSearchBody,
      search: '',
      facets: {
        Topic: ['0|Government#GOV#'],
        datasourceId: ['ds-demo-release'],
      },
    })
    .reply((_, res) => res.json(mockFacet));
  mock('search')
    .checkBody({
      ...homeSearchBody,
    })
    .reply((_, res) => res.json(mockHomeEn));
  mock('search')
    .checkBody({
      ...homeSearchBody,
      lang: 'fr',
    })
    .reply((_, res) => res.json(mockHomeFr));
  mock('search')
    .checkBody({
      ...isIndexedBody,
      search:
        'dataflowId:"DF_DOMESTIC_TOURISM_DEMO_INDICATOR" version:"5.0" agencyId:"OECD.CFE"',
    })
    .reply((_, res) => res.json(isIndexedResponse(true)));
  mock('search')
    .checkBody({
      ...isIndexedBody,
      search:
        'dataflowId:"DF_OUTBOUND_TOURISM" version:"5.0" agencyId:"OECD.CFE"',
    })
    .reply((_, res) => res.json(isIndexedResponse(true)));
  mock('search')
    .checkBody({
      ...isIndexedBody,
      search:
        'dataflowId:"DF_DOMESTIC_TOURISM" version:"5.0" agencyId:"OECD.CFE"',
    })
    .reply((_, res) => res.json(isIndexedResponse(false)));
  mock('search')
    .checkBody({
      ...isIndexedBody,
      search:
        'dataflowId:"DF_INBOUND_TOURISM" version:"5.0" agencyId:"OECD.CFE"',
    })
    .reply((_, res) => res.json(isIndexedResponse(false)));
});
