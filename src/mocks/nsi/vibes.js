const { vibe } = require('farso');
const { set, lensPath } = require('ramda');
const snaStructure = require('./mocks/sna-structure.json');
const snaData = require('./mocks/sna-data.json');
const snaContraint = require('./mocks/sna-constraint.json');
const snaMetadata = require('./mocks/sna-metadata.json');
const snaMetadataStructure = require('./mocks/sna-metadata.json');
const itcsStructure = require('./mocks/itcs-structure.json');
const itcsData = require('./mocks/itcs-data.json');
const itcsConstraint = require('./mocks/itcs-constraint.json');
const eonStructure = require('./mocks/eon/structure.json');
const eonData = require('./mocks/eon/data.json');
const eonAvailableConstraint = require('./mocks/eon/available-constraint.json');
const qnaCombStructure = require('./mocks/qna_comb-structure.json');
const qnaCombMeasureHcoldelist = require('./mocks/qna_comb-h_codelist-measure.json');
const qnaCombSectorHcoldelist = require('./mocks/qna_comb-h_codelist-sector.json');
const qnaCombData = require('./mocks/qna_comb-data.json');
const qnaCombConstraint = require('./mocks/qna_comb-constraint.json');
const ddownStructure = require('./mocks/ddown-structure.json');
const ddownData = require('./mocks/ddown-data.json');
const ddownConstraint = require('./mocks/ddown-constraint.json');
const ddownMicrodata = require('./mocks/ddown-microdata.json');
const typesStructure = require('./mocks/types-structure.json');
const typesData = require('./mocks/types-data.json');
const typesConstraint = require('./mocks/types-constraint.json');
const noTimeStructure = require('./mocks/no-time-structure.json');
const noTimeData = require('./mocks/no-time-data.json');
const noTimeConstraint = require('./mocks/no-time-constraint.json');
const noTimeMetadataStructure = require('./mocks/no-time-metadata-structure.json');
const noTimeMetadata = require('./mocks/no-time-metadata.json');
const irregularStructure = require('./mocks/irregular-structure.json');
const irregularData = require('./mocks/irregular-data.json');
const irregularConstraint = require('./mocks/irregular-constraint.json');
const perfStructure = require('./mocks/perf/structure.json');
const perfData = require('./mocks/perf/data.json');
const perfConstraint = require('./mocks/perf/constraints.json');
const crs1Structure = require('./mocks/crs1-structure.json');
const crs1HCodelistRecipient = require('./mocks/crs1-h_codelist-recipient.json');
const crs1Constraint = require('./mocks/crs1-constraint.json');
const naseStructure = require('./mocks/nase/structure.json');
const naseData = require('./mocks/nase/data.json');
const multipleFreqStructure = require('./mocks/multiple-freq-structure.json');
const multipleFreqData = require('./mocks/multiple-freq-data.json');
const multipleFreqConstraints = require('./mocks/multiple-freq-constraints.json');
const relatedDfsStructure = require('./mocks/relatedDfs/structure.json');
const relatedDfsStructureParent = require('./mocks/relatedDfs/structure-parents.json');
const relatedDfsData = require('./mocks/relatedDfs/data.json');

vibe('sna', mock => {
  mock('sna:structure').reply((_, res) => res.json(snaStructure));
  mock('sna:data').reply((_, res) => res.json(snaData));
  mock('sna:data:constraint').reply((_, res) => res.json(snaContraint));
  mock('sna:metadata').reply((_, res) => res.json(snaMetadata));
  mock('sna:metadata:structure').reply((_, res) =>
    res.json(snaMetadataStructure),
  );
});

vibe('sna:content-range', mock => {
  mock('sna:structure').reply((_, res) => res.json(snaStructure));
  mock('sna:data').reply((_, res) => {
    res.set('content-range', 'values 0-9/20');
    return res.json(snaData);
  });
  mock('sna:data:constraint').reply((_, res) => res.json(snaContraint));
});

vibe('itcs', mock => {
  mock('itcs:structure').reply((_, res) => res.json(itcsStructure));
  mock('itcs:data').reply((_, res) => res.json(itcsData));
  mock('itcs:constraint').reply((_, res) => res.json(itcsConstraint));
});

vibe('oecd', mock => {
  mock('eon:structure').reply((_, res) => res.json(eonStructure));
  mock('eon:data').reply((_, res) => res.json(eonData));
  mock('eon:availableconstraint').reply((_, res) =>
    res.json(eonAvailableConstraint),
  );
});

vibe.default('qna_comb', mock => {
  mock('qna_comb:structure').reply((_, res) => res.json(qnaCombStructure));
  mock('qna_comb:hierarchicalcodelist:sector').reply((_, res) =>
    res.json(qnaCombSectorHcoldelist),
  );
  mock('qna_comb:hierarchicalcodelist:measure').reply((_, res) =>
    res.json(qnaCombMeasureHcoldelist),
  );
  mock('qna_comb:data').reply((_, res) => res.json(qnaCombData));
  mock('qna_comb:availableconstraint').reply((_, res) =>
    res.json(qnaCombConstraint),
  );
});

vibe('ddown', mock => {
  mock('ddown:structure').reply((_, res) => res.json(ddownStructure));
  mock('ddown:data').reply((_, res) => res.json(ddownData));
  mock('ddown:availableconstraint').reply((_, res) =>
    res.json(ddownConstraint),
  );
  mock('ddown:microdata').reply((_, res) => res.json(ddownMicrodata));
});

vibe('types', mock => {
  mock('types:structure').reply((_, res) => res.json(typesStructure));
  mock('types:data').reply((_, res) => res.json(typesData));
  mock('types:availableconstraint').reply((_, res) =>
    res.json(typesConstraint),
  );
});

vibe('types:boolean', mock => {
  mock('types:structure').reply((_, res) =>
    res.json(
      set(
        lensPath([
          'data',
          'dataStructures',
          0,
          'dataStructureComponents',
          'measureList',
          'primaryMeasure',
          'localRepresentation',
          'textFormat',
          'textType',
        ]),
        'Boolean',
        typesStructure,
      ),
    ),
  );
  mock('types:data').reply((_, res) => res.json(typesData));
  mock('types:availableconstraint').reply((_, res) =>
    res.json(typesConstraint),
  );
});

vibe('no_time', mock => {
  mock('no_time:structure').reply((_, res) => res.json(noTimeStructure));
  mock('no_time:data').reply((_, res) => res.json(noTimeData));
  mock('no_time:constraint').reply((_, res) => res.json(noTimeConstraint));
  mock('no_time:metadata:structure').reply((_, res) =>
    res.json(noTimeMetadataStructure),
  );
  mock('no_time:metadata').reply((_, res) => res.json(noTimeMetadata));
});

vibe('irregular', mock => {
  mock('irregular:structure').reply((_, res) => res.json(irregularStructure));
  mock('irregular:data').reply((_, res) => res.json(irregularData));
  mock('irregular:constraint').reply((_, res) => res.json(irregularConstraint));
});

vibe('perf', mock => {
  mock('perf:structure').reply((_, res) => res.json(perfStructure));
  mock('perf:data').reply((_, res) => res.json(perfData));
  mock('perf:constraint').reply((_, res) => res.json(perfConstraint));
});

vibe('crs1', mock => {
  mock('crs1:structure').reply((_, res) => res.json(crs1Structure));
  mock('crs1:hierarchicalcodelist:recipient').reply((_, res) =>
    res.json(crs1HCodelistRecipient),
  );
  mock('crs1:availableconstraint').reply((_, res) => res.json(crs1Constraint));
});

vibe('nase', mock => {
  mock('nase:structure').reply((_, res) => res.json(naseStructure));
  mock('nase:data').reply((_, res) => res.json(naseData));
});

vibe('freq', mock => {
  mock('freq:structure').reply((_, res) => res.json(multipleFreqStructure));
  mock('freq:data').reply((_, res) => res.json(multipleFreqData));
  mock('freq:availableconstraint').reply((_, res) =>
    res.json(multipleFreqConstraints),
  );
});

vibe('relatedDfs', mock => {
  mock('relatedDfs:structure').reply((_, res) => res.json(relatedDfsStructure));
  mock('relatedDfs:parents').reply((_, res) =>
    res.json(relatedDfsStructureParent),
  );
  mock('relatedDfs:data').reply((_, res) => res.json(relatedDfsData));
});
