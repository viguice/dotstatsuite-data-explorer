import axios from 'axios';
import initHttp from '../init/http';
import initRouter from '../init/router';
import initServices from '../services';
import initConfig from '../init/config';
import initAssets from '../init/assets';

let CTX;

jest.mock('../configProvider', () => () => ({
  getTenants: () => Promise.resolve({ testTenant: { id: 'testTenant' } }),
  getTenant: () => Promise.resolve({ id: 'testTenant' }),
}));

describe('Main', function() {
  beforeAll(() =>
    initConfig()
      .then(initAssets)
      .then(initServices)
      .then(initRouter)
      .then(initHttp)
      .then(ctx => (CTX = ctx)),
  );

  afterAll(() => CTX.httpServer.close());

  it('should ping', () => {
    const url = `${CTX.httpServer.url}/api/healthcheck`;
    return axios({ url }).then(({ data }) => {
      expect(data.status).toEqual('OK');
      expect(data.configProvider.status).toEqual('OK');
    });
  });
});
