import debug from '../debug';
import params from '../params';
import ConfigProvider from '../configProvider';

const init = () => {
  debug.info(params, 'running config');
  return Promise.resolve({ config: params, configProvider: ConfigProvider(params) });
};

export default init;
