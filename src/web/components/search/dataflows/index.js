import React from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import { useIntl } from 'react-intl';
import { Dataflow } from '@sis-cc/dotstatsuite-visions';
import {
  getSDMXUrl,
  parseExternalReference,
} from '@sis-cc/dotstatsuite-sdmxjs';
import { makeStyles } from '@material-ui/core/styles';
import { formatMessage, FormattedMessage } from '../../../i18n';
import { setHighlights } from '../utils';
import {
  downloadableDataflowResults,
  getSpace,
  getSpaceFromUrl,
  search,
} from '../../../lib/settings';
import Downloads from './downloads';
import SanitizedInnerHTML from '../../SanitizedInnerHTML';
import { useDispatch, useSelector } from 'react-redux';
import { getPending } from '../../../selectors/app';
import {
  requestSearchDataFile,
  changeDataflow,
  requestExternalResources,
} from '../../../ducks/sdmx';
import { visDlMessages } from '../../messages';
import { getFilename } from '../../../lib/sdmx';

const useStyles = makeStyles(theme => ({
  root: {
    borderTop: `1px solid ${theme.palette.grey[700]}`,
  },
}));

const getCsvFileUrl = dataflow => {
  const externalReference = parseExternalReference(
    {
      isExternalReference: !R.isNil(dataflow.externalUrl),
      links: [{ rel: 'external', href: dataflow.externalUrl }],
    },
    'dataflow',
  );
  const identifiers = { code: R.prop('dataflowId', dataflow), ...dataflow };
  const space = R.isNil(externalReference)
    ? getSpace(R.prop('datasourceId', dataflow))
    : getSpaceFromUrl(R.path(['datasource', 'url'], externalReference)) ||
      R.prop('datasource', externalReference);

  const url = getSDMXUrl({
    datasource: space,
    identifiers: R.defaultTo(
      identifiers,
      R.prop('identifiers', externalReference),
    ),
    type: 'data',
  });
  return `${url}&format=csvfile`;
};

const Dataflows = ({ dataflows }) => {
  const classes = useStyles();
  const intl = useIntl();
  const dispatch = useDispatch();
  const isAppPending = useSelector(getPending); // downloads

  const handleChangeDataflow = (...args) => dispatch(changeDataflow(...args));
  const handleRequestExternalResources = (...args) =>
    dispatch(requestExternalResources(...args));

  return (
    <div className={classes.root} data-testid="search_results">
      {R.map(
        dataflow => (
          <Dataflow
            {...dataflow}
            HTMLRenderer={SanitizedInnerHTML}
            testId={R.prop('id', dataflow)}
            key={R.prop('id', dataflow)}
            id={R.prop('id', dataflow)}
            title={R.prop('name', dataflow)}
            body={{ description: R.prop('description', dataflow) }}
            url={R.prop('url', dataflow)}
            handleUrl={event => {
              event.stopPropagation();
              if (event.ctrlKey) return;
              event.preventDefault();
              return handleChangeDataflow(dataflow);
            }}
            highlights={R.pipe(
              R.prop('highlights'),
              R.map(setHighlights({ intl })),
            )(dataflow)}
            label={
              R.includes(
                'datasourceId',
                R.propOr([], 'excludedFacetIds', search),
              )
                ? null
                : R.prop('datasourceId', dataflow)
            }
            labels={{
              dimensions: (
                <FormattedMessage id="de.search.dataflow.dimensions" />
              ),
              source: <FormattedMessage id="de.data.source" />,
              lastUpdated: (
                <FormattedMessage id="de.search.dataflow.last.updated" />
              ),
              note: `${R.prop('agencyId', dataflow)}:${R.prop(
                'dataflowId',
                dataflow,
              )}(${R.prop('version', dataflow)})`,
              date: intl.formatDate(R.prop('lastUpdated', dataflow), {
                year: 'numeric',
                month: 'long',
                day: '2-digit',
                hour: 'numeric',
                minute: 'numeric',
                second: 'numeric',
              }),
            }}
          >
            {downloadableDataflowResults && (
              <Downloads
                downloads={[
                  {
                    id: 'csv.all',
                    callback: () => {
                      //dispatch as callback for google analytics
                      dispatch(
                        requestSearchDataFile({
                          dataflow,
                          isDownloadAllData: true,
                        }),
                      );
                    },
                    label: formatMessage(intl)(
                      R.prop('data.download.csv.all', visDlMessages),
                    ),
                    link: getCsvFileUrl(dataflow),
                    filename: getFilename({
                      isFull: true,
                      identifiers: {
                        code: R.prop('dataflowId', dataflow),
                        ...dataflow,
                      },
                    }),
                  },
                ]}
                isDownloading={R.prop(`getDataFile/${R.prop('id', dataflow)}`)(
                  isAppPending,
                )}
                isExternalLoading={R.prop(
                  `getExternalResources/${R.prop('dataflowId', dataflow)}`,
                )(isAppPending)}
                action={() => handleRequestExternalResources({ dataflow })}
                dataflowId={R.prop('dataflowId', dataflow)}
                externalMessage={
                  <FormattedMessage id="de.no.external.resources" />
                }
                pending={isAppPending}
              />
            )}
          </Dataflow>
        ),
        dataflows,
      )}
    </div>
  );
};

Dataflows.propTypes = {
  dataflows: PropTypes.array,
};

export default Dataflows;
