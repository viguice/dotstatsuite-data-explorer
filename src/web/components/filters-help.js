import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useIntl } from 'react-intl';
import { Tooltip } from '@sis-cc/dotstatsuite-visions';
import HelpIcon from '@material-ui/icons/EmojiObjects';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import LensIcon from '@material-ui/icons/Lens';
import { FormattedMessage, formatMessage } from '../i18n';
import messages from './messages';
import useEventListener from '../utils/useEventListener';

const FILTERS_ID = 'filters';

const useStyles = makeStyles(() => ({
  root: {
    padding: 0,
    fontFamily: 'Roboto Slab, serif',
    fontSize: '17px',
    color: 'Body',
  },
}));

const FiltersHelp = ({ isSearch }) => {
  const classes = useStyles();
  const intl = useIntl();
  const [open, setOpen] = useState(false);

  const onOpen = () => setOpen(true);
  const onClose = () => setOpen(false);

  const handler = React.useCallback(e => {
    if (e.key === 'Escape') {
      onClose();
    }
  }, []);

  useEventListener('keydown', handler);
  return (
    <Grid container data-testid="filters-help">
      <Typography
        variant="body2"
        className={classes.root}
        id={FILTERS_ID}
        tabIndex={0}
      >
        <FormattedMessage id="de.side.filters.action" />
        &nbsp;
      </Typography>
      {isSearch && (
        <Tooltip
          variant="light"
          tabIndex={0}
          aria-label={formatMessage(intl)(messages.help)}
          aria-hidden={false}
          placement="bottom-start"
          title={
            <Typography id="filtersHelpers" variant="body2">
              <FormattedMessage
                id="de.filters.search.help"
                values={{ icon: <LensIcon style={{ fontSize: 5 }} /> }}
              />
            </Typography>
          }
          interactive
          open={open}
          onOpen={onOpen}
          onClose={onClose}
        >
          <HelpIcon fontSize="small" />
        </Tooltip>
      )}
    </Grid>
  );
};

FiltersHelp.propTypes = {
  isSearch: PropTypes.bool,
};

export default FiltersHelp;
