import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useIntl } from 'react-intl';
import IconButton from '@material-ui/core/IconButton';
import Container from '@material-ui/core/Container';
import {
  AccessibilityFilled,
  AccessibilityOutlined,
  Tooltip,
} from '@sis-cc/dotstatsuite-visions';
import { FormattedMessage, formatMessage } from '../i18n';
import { getHasAccessibility } from '../selectors/router';
import { changeHasAccessibility } from '../ducks/app';
import messages from './messages';

const AccessibilityButton = () => {
  const dispatch = useDispatch();
  const intl = useIntl();
  const hasAccessibility = useSelector(getHasAccessibility);

  return (
    <Tooltip
      placement="bottom"
      variant="light"
      title={
        <Container>
          {hasAccessibility ? (
            <FormattedMessage id="accessibility.disable" />
          ) : (
            <FormattedMessage id="accessibility.enable" />
          )}
        </Container>
      }
      interactive
    >
      <IconButton
        aria-label={formatMessage(intl)(messages.accessibilitySupport)}
        aria-pressed={hasAccessibility}
        onClick={() => dispatch(changeHasAccessibility(!hasAccessibility))}
      >
        {hasAccessibility ? (
          <AccessibilityFilled color="primary" />
        ) : (
          <AccessibilityOutlined color="primary" />
        )}
      </IconButton>
    </Tooltip>
  );
};

export default AccessibilityButton;
