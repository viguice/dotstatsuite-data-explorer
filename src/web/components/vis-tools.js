import React from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import * as R from 'ramda';
import ToolBar from './ToolBar';
import Collapse from '@material-ui/core/Collapse';
import { makeStyles } from '@material-ui/core';
import { getVisActionId } from '../selectors';
import { getViewer } from '../selectors/router';
import APIQueries from './vis/api-queries';
import ShareView from './vis/share';
import { getUser } from '../selectors/app';
import {
  TABLE,
  MARGE_SIZE,
  MARGE_RATIO,
  SIDE_WIDTH,
  SMALL_SIDE_WIDTH,
} from '../utils/constants';
import { getIsMicrodata } from '../selectors/microdata';
import Panel from './vis/Panel';
import Config from './vis-tools/config';
import Table from './vis-tools/table';

// maxWidth - maxWidth * (1 - MARGE_RATIO)- SMALL_SIDE_WIDTH - 16
const useStyles = makeStyles(theme => ({
  toolbar: {
    position: 'sticky',
    borderTop: `solid 2px ${theme.palette.grey[700]}`,
    [theme.breakpoints.down('xs')]: {
      maxWidth: 'none',
    },
    [theme.breakpoints.between('sm', 'md')]: {
      ['left']: `${MARGE_SIZE}%`,
      maxWidth: ({ visWidth }) => visWidth,
    },
    [theme.breakpoints.only('md')]: {
      ['left']: ({ isFull }) =>
        isFull
          ? `${MARGE_SIZE}%`
          : `calc(${MARGE_SIZE}% + ${SMALL_SIDE_WIDTH + theme.spacing(2)}px)`,
      maxWidth: ({ visWidth, isFull }) =>
        visWidth - (isFull ? 0 : SMALL_SIDE_WIDTH) - theme.spacing(2),
    },
    [theme.breakpoints.up('lg')]: {
      ['left']: ({ isFull }) =>
        isFull
          ? `${MARGE_SIZE}%`
          : `calc(${MARGE_SIZE}% + ${SIDE_WIDTH + theme.spacing(2)}px)`,
      maxWidth: ({ visWidth, isFull }) =>
        visWidth - (isFull ? 0 : SIDE_WIDTH) - theme.spacing(2),
    },
  },
}));

const API = 'api';
const CONFIG = 'config';
const SHARE = 'share';

const Tools = ({ maxWidth, isFull, viewerProps, properties = {} }) => {
  const isMicrodata = useSelector(getIsMicrodata);
  const actionId = useSelector(getVisActionId());
  const viewerId = useSelector(getViewer);
  const isAuthenticated = !!useSelector(getUser);

  const classes = useStyles({
    visWidth: maxWidth - maxWidth * (1 - MARGE_RATIO),
    isFull: R.or(isFull, isMicrodata),
  });

  const isApi = R.equals(API)(actionId);
  const isTableConfig = R.equals(CONFIG)(actionId) && R.equals(TABLE)(viewerId);
  const isChartConfig =
    R.equals(CONFIG)(actionId) && !R.equals(TABLE)(viewerId);
  const isShare = R.equals(SHARE)(actionId);

  return (
    <div className={classes.toolbar}>
      <ToolBar viewerProps={viewerProps} />
      <Collapse in={isApi}>
        <Panel actionId={API}>
          <APIQueries />
        </Panel>
      </Collapse>
      <Collapse in={isTableConfig}>
        <Panel actionId={CONFIG}>
          <Table />
        </Panel>
      </Collapse>
      <Collapse in={isChartConfig}>
        <Panel actionId={CONFIG}>
          <Config isAuthenticated={isAuthenticated} properties={properties} />
        </Panel>
      </Collapse>
      <Collapse in={isShare}>
        {isShare && (
          <Panel actionId={SHARE}>
            <ShareView viewerProps={viewerProps} />
          </Panel>
        )}
      </Collapse>
    </div>
  );
};

Tools.propTypes = {
  maxWidth: PropTypes.number,
  isFull: PropTypes.bool,
  viewerProps: PropTypes.object,
  properties: PropTypes.object,
};

export default Tools;
