import React from 'react';
import * as R from 'ramda';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import MuiInfoIcon from '@material-ui/icons//Info';
import IconButton from '@material-ui/core/IconButton';
import { toggleMetadata } from '../../ducks/metadata';
import { getCoordinates } from '../../selectors/metadata';

const useStyles = makeStyles(theme => ({
  icon: {
    color: theme.palette.primary.main,
    backgroundColor: 'inherit',
    padding: 0,
  },
  selected: {
    color: theme.palette.highlight.hl1,
    backgroundColor: 'black',
    padding: 0,
  },
}));

const Icon = ({ sideProps }) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const selectedCoordinates = useSelector(getCoordinates);
  const onClick = () => {
    dispatch(toggleMetadata(sideProps));
  };
  const isSelected = R.equals(
    R.prop('coordinates', sideProps || {}),
    selectedCoordinates,
  );
  return (
    <IconButton
      size="small"
      className={isSelected ? classes.selected : classes.icon}
      onClick={onClick}
      data-testid="ref-md-info"
      aria-pressed={isSelected}
      aria-label={`metadata-${R.join('-', R.values(sideProps.coordinates))}`}
    >
      <MuiInfoIcon fontSize="small" />
    </IconButton>
  );
};

Icon.propTypes = {
  sideProps: PropTypes.object,
};

const MetadataIcon = ({ sideProps }) => {
  if (R.isNil(sideProps)) {
    return null;
  }
  return <Icon sideProps={sideProps} />;
};

MetadataIcon.propTypes = {
  sideProps: PropTypes.object,
};

export default MetadataIcon;
