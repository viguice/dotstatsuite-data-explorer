import React from 'react';
import PropTypes from 'prop-types';
import Accordion from '@material-ui/core/Accordion';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { makeStyles } from '@material-ui/core/styles';
import EmojiObjectsIcon from '@material-ui/icons/EmojiObjects';
import { FormattedMessage } from '../../i18n';
import * as R from 'ramda';
import { guidancesMessages } from '../messages';
import { MARGE_RATIO, MARGE_SIZE } from '../../utils/constants';

const groupedGuidanceMessages = {
  BarChart: guidancesMessages.bar,
  RowChart: guidancesMessages.row,
  ScatterChart: guidancesMessages.scatter,
  HorizontalSymbolChart: guidancesMessages.hsymbol,
  VerticalSymbolChart: guidancesMessages.vsymbol,
  TimelineChart: guidancesMessages.timeline,
  StackedBarChart: guidancesMessages.stackedBar,
  StackedRowChart: guidancesMessages.stackedRow,
  ChoroplethChart: guidancesMessages.choro,
};

const useStyles = makeStyles(() => ({
  container: {
    position: 'sticky',
    left: `${MARGE_SIZE}%`,
  },
  accordion: {
    boxShadow: 'none',
  },
  guidances: {
    display: 'flex',
    flexDirection: 'column',
  },
}));

const Guidances = ({ type, maxWidth = 0 }) => {
  const contentMessage = R.prop(type, groupedGuidanceMessages);
  if (R.isNil(contentMessage)) {
    return null;
  }
  const classes = useStyles();
  const summaryMessage = type === 'table' ? guidancesMessages.table : guidancesMessages.chart;
  return (
    <div className={classes.container} style={{ maxWidth: maxWidth * MARGE_RATIO }}>
      <Accordion className={classes.accordion}>
        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
          <EmojiObjectsIcon fontSize="small" />
          <FormattedMessage {...summaryMessage} />
        </AccordionSummary>
        <AccordionDetails className={classes.guidances}>
          <Typography variant="body2">
            <FormattedMessage {...contentMessage} />
          </Typography>
        </AccordionDetails>
      </Accordion>
    </div>
  );
};

Guidances.propTypes = {
  type: PropTypes.string.isRequired,
  maxWidth: PropTypes.number,
};

export default Guidances;
