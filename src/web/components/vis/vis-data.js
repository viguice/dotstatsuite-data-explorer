import React, { Fragment, useState, useCallback, Profiler } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useIntl } from 'react-intl';
import PropTypes from 'prop-types';
import {
  rules,
  Viewer as ViewerComp,
  rules2,
} from '@sis-cc/dotstatsuite-components';
import { isTimePeriodDimension } from '@sis-cc/dotstatsuite-sdmxjs';
import Link from '@material-ui/core/Link';
import * as R from 'ramda';
import * as Settings from '../../lib/settings';
import { getIsPending } from '../../selectors/app';
import { getIsFull, getIsIncreased, getVisIsLoadingMap } from '../../selectors';
import { getHasMicrodata } from '../../selectors/microdata';
import {
  getDisplay,
  getIsRtl,
  getLocale,
  getViewer,
} from '../../selectors/router';
import {
  getDuplicatedObservations,
  getDefaultTitleLabel,
  getHeaderSubtitle,
  getHeaderTitle,
  getVisDataflow,
  getHeaderCombinations,
  getHeaderProps,
} from '../../selectors/data';
import {
  getActualConstraints,
  getData,
  getDataRequestRange,
  getObservationsCount,
  getObservationsType,
  getRefinedDataRange,
  getTextAlign,
  getUsedFilters,
} from '../../selectors/sdmx';
import Guidances from './guidances';
import MetadataIcon from './MetadataIcon';
import MetaDataDrawer from './meta-data-drawer';
import MicrodataViewer from './microdata';
import Overview from './overview';
import Tools from '../vis-tools';
import { FormattedMessage, formatMessage } from '../../i18n';
import SanitizedInnerHTML from '../SanitizedInnerHTML';
import messages from '../messages';
import { ID_VIEWER_COMPONENT } from '../../css-api';
import { getHeaderSideProps } from '../../selectors/metadata';
import { updateMicrodataConstraints } from '../../ducks/microdata';
import {
  getChartConfigState,
  getChartData,
  getChartOptions,
  getHasNeedOfComputedAxis,
  getHasNeedOfResponsiveSize,
  getHasNoTime,
} from '../../selectors/chart';
import { UPDATE_ATTEMPT_CHART_CONFIG } from '../../ducks/chart-configs';
import { getIsDefaultInformations } from '../../utils/viewer';
import useSdmxData from '../../hooks/useSdmxData';
import { getLayoutData, getCuratedCells } from '../../selectors/table';
import { LEFT, RIGHT } from '../../utils/constants';
import { profilerOnRender } from '../../lib/perf';

const getTableHeaderDisclaimer = (
  intl,
  range,
  isTruncated,
  totalCells,
  cellsLimit,
) => {
  const { count, total } = range;
  if (count < total || isTruncated) {
    const values = {
      cellsLimit,
      total,
      totalCells,
      type: 'table',
    };
    if (total > cellsLimit) {
      return formatMessage(intl)(messages.visDataLimit, values);
    }
    return formatMessage(intl)(messages.tableCellsLimit, values);
  }
  return null;
};

const getChartHeaderDisclaimer = (intl, range, dataLimit) => {
  const { count, total } = range;
  if (count < total) {
    const limit = R.isNil(dataLimit) ? count : dataLimit;
    const values = {
      cellsLimit: limit,
      total,
      type: 'chart',
    };
    return formatMessage(intl)(messages.visDataLimit, values);
  }
  return null;
};

const TableVisualisation = ({
  isFull,
  maxWidth,
  loading,
  loadingProps,
  noData,
  errorMessage,
  footerProps,
}) => {
  const dispatch = useDispatch();
  const intl = useIntl();
  const [activeCellCoordinates, setCellCoordinates] = useState({});
  const isRtl = useSelector(getIsRtl);

  //--------------------------------------------------------------------
  const layoutData = useSelector(getLayoutData);
  const cells = useSelector(getCuratedCells);
  //--------------------------------------------------------------------

  const display = useSelector(getDisplay);

  const hasMicrodata = useSelector(getHasMicrodata);
  const observationsType = useSelector(getObservationsType);
  const cellsLimit = useSelector(getDataRequestRange);
  const range = useSelector(getRefinedDataRange);
  const textAlign = useSelector(getTextAlign);

  const getBooleanValue = value => {
    if (value === 'true' || value === '1') {
      return true;
    }
    return false;
  };

  const formatBoolean = value =>
    value ? (
      <FormattedMessage id="sdmx.data.true" />
    ) : (
      <FormattedMessage id="sdmx.data.false" />
    );

  const cellValueAccessor = value => {
    if (R.is(Boolean, value)) return formatBoolean(value);
    else if (observationsType === 'Boolean')
      return formatBoolean(getBooleanValue(value));
    return value;
  };

  const labelAccessor = useCallback(rules2.getTableLabelAccessor(display), [
    display,
  ]);

  const tableProps = {
    ...layoutData,
    cells,
    SideIcon: MetadataIcon,
    labelAccessor,
    cellValueAccessor,
    activeCellIds: activeCellCoordinates,
    activeCellHandler: setCellCoordinates,
    HTMLRenderer: SanitizedInnerHTML,
    cellHandler: hasMicrodata
      ? ({ indexedDimValIds }) =>
          dispatch(updateMicrodataConstraints(indexedDimValIds))
      : null,
    isNoWrap: isTimePeriodDimension,
    textAlign: R.prop(textAlign)({ RIGHT, LEFT }),
  };
  const { truncated, totalCells } = layoutData;
  const disclaimer = getTableHeaderDisclaimer(
    intl,
    range,
    truncated,
    totalCells,
    cellsLimit,
  );

  const _headerProps = useSelector(getHeaderProps);
  const headerSideProps = useSelector(getHeaderSideProps);
  const headerProps = {
    ..._headerProps,
    isSticky: true,
    disclaimer,
    sideProps: headerSideProps,
    SideIcon: MetadataIcon,
  };

  const viewerProps = {
    cellsLimit,
    range,
    isRtl,
    footerProps,
    headerProps,
    tableProps,
    type: 'table',
  };

  // importing afterFrame works in e2e and in browser
  // but doesn't work in unit test env, MessageChannel is not defined
  // mocking is not working...
  //const perf = perfToMeasure(ID_VIEWER_COMPONENT);
  //afterFrame(() => {
  //  perf.end();
  //});

  return (
    <Fragment>
      <Tools maxWidth={maxWidth} isFull={isFull} viewerProps={viewerProps} />
      <div id={ID_VIEWER_COMPONENT}>
        <Profiler id={ID_VIEWER_COMPONENT} onRender={profilerOnRender}>
          <ViewerComp
            {...viewerProps}
            errorMessage={errorMessage}
            loading={loading}
            loadingProps={loadingProps}
            noData={noData}
          />
        </Profiler>
      </div>
      <Guidances type="table" maxWidth={maxWidth} />
    </Fragment>
  );
};

TableVisualisation.propTypes = {
  isFull: PropTypes.bool,
  maxWidth: PropTypes.number,
  footerProps: PropTypes.object,
  errorMessage: PropTypes.element,
  noData: PropTypes.element,
  loadingProps: PropTypes.object,
  loading: PropTypes.element,
};

const ChartVisualisation = ({
  type,
  isFull,
  maxWidth,
  loading,
  loadingProps,
  noData,
  errorMessage,
  footerProps,
  locale,
}) => {
  const intl = useIntl();
  const dispatch = useDispatch();
  const data = useSelector(getData);
  const dataflow = useSelector(getVisDataflow);

  const headerTitle = useSelector(getHeaderTitle);
  const headerSubtitle = useSelector(getHeaderSubtitle);
  const headerCombinations = useSelector(getHeaderCombinations);

  const cellsLimit = useSelector(getDataRequestRange);
  const range = useSelector(getRefinedDataRange);
  const disclaimer = getChartHeaderDisclaimer(intl, range, cellsLimit);
  const headerSideProps = useSelector(getHeaderSideProps);
  const headerProps = {
    isSticky: true,
    title: headerTitle,
    subtitle: headerSubtitle,
    disclaimer,
    combinations: headerCombinations,
    sideProps: headerSideProps,
    SideIcon: MetadataIcon,
  };

  const chartData = useSelector(getChartData);
  const chartOptions = useSelector(getChartOptions);
  const onChangeConfig = payload =>
    dispatch({ type: UPDATE_ATTEMPT_CHART_CONFIG, payload });
  const hasNeedOfResponsiveSize = useSelector(getHasNeedOfResponsiveSize);
  const hasNeedOfComputedAxis = useSelector(getHasNeedOfComputedAxis);

  const isLoadingData = useSelector(getIsPending('getData'));
  const getResponsiveSize =
    hasNeedOfResponsiveSize && !isLoadingData ? onChangeConfig : null;
  const getAxisOptions =
    hasNeedOfComputedAxis && !isLoadingData ? onChangeConfig : null;

  const chartConfig = useSelector(getChartConfigState);
  const properties = rules.toProperties(
    {
      data,
      dataflow,
      type,
      options: Settings.chartOptions,
      subtitle: headerSubtitle,
      title: headerTitle,
    },
    chartConfig,
    onChangeConfig,
  );

  const { sourceLabel, title, subtitle, withLogo, withCopyright } = chartConfig;
  const _footerProps = R.pipe(
    R.when(R.always(!withLogo), R.dissoc('logo')),
    R.when(R.always(!withCopyright), R.dissoc('copyright')),
    R.when(
      R.always(!R.isNil(sourceLabel) && !R.isEmpty(sourceLabel)),
      R.assocPath(['source', 'label'], sourceLabel),
    ),
  )(footerProps);

  const _headerProps = R.pipe(
    R.when(
      R.always(!R.isNil(title) && !R.isEmpty(title)),
      R.assocPath(['title', 'label'], title),
    ),
    R.when(
      R.always(!R.isNil(subtitle) && !R.isEmpty(subtitle)),
      R.assoc('subtitle', [{ label: subtitle }]),
    ),
  )(headerProps);

  const viewerProps = {
    isDefaultTitle: !R.isNil(title) && !R.isEmpty(title),
    isDefaultSubtitle: !R.isNil(subtitle) && !R.isEmpty(subtitle),
    isDefaultSourceLabel: !R.isNil(sourceLabel) && !R.isEmpty(sourceLabel),
    cellsLimit,
    range,
    headerProps: _headerProps,
    footerProps: _footerProps,
    chartData,
    chartOptions,
    type,
    ...getIsDefaultInformations(properties),
  };
  return (
    <Fragment>
      <Tools
        maxWidth={maxWidth}
        isFull={isFull}
        properties={properties}
        viewerProps={viewerProps}
      />
      <div id={ID_VIEWER_COMPONENT}>
        <ViewerComp
          {...viewerProps}
          locale={locale}
          timeFormats={{ M: R.path([locale, 'timeFormat'], Settings.locales) }}
          errorMessage={errorMessage}
          getAxisOptions={getAxisOptions}
          getResponsiveSize={getResponsiveSize}
          loading={loading}
          loadingProps={loadingProps}
          noData={noData}
        />
      </div>
      <Guidances type={type} maxWidth={maxWidth} />
    </Fragment>
  );
};

ChartVisualisation.propTypes = {
  isFull: PropTypes.bool,
  maxWidth: PropTypes.number,
  type: PropTypes.string.isRequired,
};

export const ParsedDataVisualisation = ({ visPageWidth }) => {
  const type = useSelector(getViewer);
  const isFull = useSelector(getIsFull());
  const intl = useIntl();
  const locale = useSelector(getLocale);
  const isParsingData = useSelector(getIsPending('parsingData'));
  const observationsCount = useSelector(getObservationsCount);
  const actualConstraints = useSelector(getActualConstraints);
  const observations = useSelector(getDuplicatedObservations);
  const { isLoading: isFetchingData, isError, error } = useSdmxData();
  const isLoadingData = isFetchingData || isParsingData;
  const isLoadingMap = useSelector(getVisIsLoadingMap);
  const isIncreased = useSelector(getIsIncreased);
  const selection = useSelector(getUsedFilters);
  const data = useSelector(getData);
  const noObservations = R.isEmpty(observations) && !R.isNil(data);

  let loading = null;
  let loadingProps = null;
  if (isLoadingData) {
    loading = isIncreased ? (
      <FormattedMessage id="de.visualisation.data.larger.loading" />
    ) : (
      <FormattedMessage id="de.visualisation.data.loading" />
    );
    loadingProps = isIncreased ? { isWarning: true } : {};
  } else if (isLoadingMap) {
    loading = <FormattedMessage id="de.vis.map.loading" />;
  }

  const hasNoTime = useSelector(getHasNoTime);
  let noData = null;
  let errorMessage = null;
  if (isError || noObservations) {
    const status = error?.response?.status;
    if (status === 404 || noObservations) {
      if (observationsCount == 0 || R.isEmpty(actualConstraints))
        errorMessage = <FormattedMessage id="vx.no.data" />;
      else errorMessage = <FormattedMessage id="vx.no.data.selection" />;
    } else if (R.includes(status, [401, 402, 403]))
      errorMessage = <FormattedMessage id="log.error.sdmx.40x" />;
    else errorMessage = <FormattedMessage id="log.error.sdmx.xxx" />;
  } else {
    if (hasNoTime) errorMessage = <FormattedMessage id="vx.no.time.data" />;
    if (!R.isEmpty(selection))
      noData = <FormattedMessage id="vx.no.data.selection" />;
  }

  const footerProps = {
    isSticky: true,
    source: {
      label: useSelector(getDefaultTitleLabel),
      link: window.location.href,
    },
    logo: Settings.getAsset('viewerFooter', locale),
    copyright: {
      label: (
        <Link
          href={formatMessage(intl)(messages.viewerLink)}
          rel="noopener noreferrer"
          target="_blank"
          variant="body2"
        >
          <FormattedMessage id="de.viewer.copyright.label" />
        </Link>
      ),
      content: `${formatMessage(intl)(
        messages.viewerContentLabel,
      )} ${formatMessage(intl)(messages.viewerLinkLabel)}`,
    },
  };

  return (
    <Fragment>
      <MetaDataDrawer />
      {type === 'overview' && (
        <Fragment>
          <Tools maxWidth={visPageWidth} isFull={isFull} />
          <Overview />
        </Fragment>
      )}
      {type === 'table' && (
        <TableVisualisation
          maxWidth={visPageWidth}
          loading={loading}
          loadingProps={loadingProps}
          noData={noData}
          errorMessage={errorMessage}
          footerProps={footerProps}
          isFull={isFull}
        />
      )}
      {type === 'microdata' && (
        <Fragment>
          <Tools maxWidth={visPageWidth} isFull={isFull} />
          <div id={ID_VIEWER_COMPONENT}>
            <MicrodataViewer defaultFooterProps={footerProps} />
          </div>
        </Fragment>
      )}
      {type !== 'overview' && type !== 'table' && type !== 'microdata' && (
        <ChartVisualisation
          locale={locale}
          maxWidth={visPageWidth}
          isFull={isFull}
          type={type}
          loading={loading}
          loadingProps={loadingProps}
          noData={noData}
          errorMessage={errorMessage}
          footerProps={footerProps}
        />
      )}
    </Fragment>
  );
};

ParsedDataVisualisation.propTypes = {
  visPageWidth: PropTypes.number,
};
