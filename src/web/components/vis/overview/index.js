import React from 'react';
import * as R from 'ramda';
import { Link, makeStyles } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import { useIntl } from 'react-intl';
import { DataFooter } from '@sis-cc/dotstatsuite-visions';
import useOverview from '../../../hooks/useOverview';
import { formatMessage, FormattedMessage } from '../../../i18n';
import GroupLabels from './GroupLabels';
import SanitizedInnerHTML from '../../SanitizedInnerHTML';
import { ID_OVERVIEW_COMPONENT } from '../../../css-api';
import messages from '../../messages';

const useStyles = makeStyles(theme => ({
  divider: {
    backgroundColor: theme.palette.primary.light,
  },
  container: {
    margin: theme.spacing(1.25),
    [theme.breakpoints.down('xs')]: {
      margin: '10px 0 0',
    },
  },
  title: {
    fontSize: 18,
    ...R.pathOr({}, ['mixins', 'dataHeader', 'title'], theme),
  },
  textContainer: {
    margin: theme.spacing(1.25, 0, 0, 0),
  },
  listItem: {
    display: 'list-item',
    listStyle: 'disc',
  },
}));

const Overview = () => {
  const classes = useStyles();
  const intl = useIntl();
  const {
    footerProps,
    labelAccessor,
    selectedHierarchySchemes,
    externalResources,
    dataflowDescription,
    observationsCount,
    validFrom,
    oneDimensions,
    manyDimensions,
    oneAttributes,
    dataSpaceLabel,
    homeFacetIds,
    title,
    lists,
    complementaryData,
    makeHierarchyProps,
  } = useOverview();
  const enhancedFooterProps = {
    copyright: {
      label: (
        <Link
          href={formatMessage(intl)(messages.viewerLink)}
          rel="noopener noreferrer"
          target="_blank"
          variant="body2"
        >
          <FormattedMessage id="de.viewer.copyright.label" />
        </Link>
      ),
      content: `${formatMessage(intl)(
        messages.viewerContentLabel,
      )} ${formatMessage(intl)(messages.viewerLinkLabel)}`,
    },
    ...footerProps,
  };

  return (
    <>
      <Divider className={classes.divider} />
      <div className={classes.container} id={ID_OVERVIEW_COMPONENT}>
        <Typography component="h1" variant="h1" className={classes.title}>
          {title}
        </Typography>
        {!R.isNil(dataflowDescription) && (
          <div className={classes.textContainer}>
            <Typography variant="body2">
              <SanitizedInnerHTML html={dataflowDescription} />
            </Typography>
          </div>
        )}
        <div className={classes.textContainer}>
          <div>
            <GroupLabels list={oneDimensions} accessor={labelAccessor} />
          </div>
          <div>
            <GroupLabels
              list={manyDimensions}
              accessor={labelAccessor}
              isValueVisible={false}
            />
          </div>
          <div>
            <GroupLabels list={oneAttributes} accessor={labelAccessor} />
          </div>
        </div>
        <div className={classes.textContainer}>
          {R.addIndex(R.map)((hierarchies, index) => {
            const props = makeHierarchyProps(index, hierarchies);
            return (
              <GroupLabels key={`hierarchy-${index}`} isHierarchy {...props} />
            );
          }, selectedHierarchySchemes)}
          {homeFacetIds.has('datasourceId') && dataSpaceLabel && (
            <div>
              <GroupLabels list={lists['dataSource']} />
            </div>
          )}
        </div>
        <div className={classes.textContainer}>
          {!R.isNil(observationsCount) && (
            <div>
              <GroupLabels list={lists['observationsCount']} />
            </div>
          )}
          {validFrom && (
            <div>
              <GroupLabels list={lists['validFrom']} />
            </div>
          )}
        </div>
        {!R.isEmpty(externalResources) && (
          <div className={classes.textContainer}>
            <div>
              <GroupLabels
                list={lists['relatedFiles']}
                isValueVisible={false}
              />
            </div>
            <List dense>
              {R.map(
                ({ id, label, link }) => (
                  <ListItem key={id} dense>
                    <Link
                      target="_blank"
                      color="primary"
                      href={link}
                      classes={{ root: classes.listItem }}
                    >
                      {label}
                    </Link>
                  </ListItem>
                ),
                externalResources,
              )}
            </List>
          </div>
        )}
        {!R.isEmpty(complementaryData) && (
          <div
            className={classes.textContainer}
            data-testid="complementaryData"
          >
            <div>
              <GroupLabels
                list={lists['complementaryData']}
                isValueVisible={false}
              />
            </div>
            <List dense>
              {R.map(
                ({ dataflowId, label, url }) => (
                  <ListItem key={dataflowId} dense>
                    <Link
                      target="_blank"
                      color="primary"
                      href={url}
                      classes={{ root: classes.listItem }}
                    >
                      {label}
                    </Link>
                  </ListItem>
                ),
                complementaryData,
              )}
            </List>
          </div>
        )}
        <Divider className={classes.divider} />
        <DataFooter {...enhancedFooterProps} />
      </div>
    </>
  );
};

Overview.propTypes = {};

export default Overview;
