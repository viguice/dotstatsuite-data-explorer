import React from 'react';
import * as R from 'ramda';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import { Tooltip, VerticalButton } from '@sis-cc/dotstatsuite-visions';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import MuiMenu from '@material-ui/core/Menu';
import { Typography } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  button: {
    padding: theme.spacing(0.5, 0.5),
    [theme.breakpoints.down('md')]: {
      minWidth: 48,
    },
    [theme.breakpoints.down('xs')]: {
      minWidth: 36,
    },
  },
}));

export const Button = ({ children, isToolTip, ...props }) => {
  const classes = useStyles();
  const isXS = useMediaQuery(theme => theme.breakpoints.down('xs'));
  const isMD = useMediaQuery(theme => theme.breakpoints.only('md'));

  return (
    <Tooltip
      variant="light"
      tabIndex={0}
      aria-label={
        isToolTip && R.is(String)(children) && (isXS || isMD) ? children : ''
      }
      aria-hidden={false}
      placement="top"
      PopperProps={{
        modifiers: {
          flip: {
            behavior: ['top', 'bottom', 'right', 'left'],
          },
        },
      }}
      title={
        isToolTip && (isXS || isMD) ? (
          <Typography id="devisLabels" variant="body2">
            {children}
          </Typography>
        ) : (
          ''
        )
      }
    >
      <VerticalButton className={classes.button} color="primary" {...props}>
        {isXS || isMD ? null : children}
      </VerticalButton>
    </Tooltip>
  );
};

Button.propTypes = {
  children: PropTypes.oneOfType([PropTypes.node, PropTypes.string]),
  isToolTip: PropTypes.bool,
};

export const Menu = props => (
  <MuiMenu
    keepMounted
    getContentAnchorEl={null}
    anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
    transformOrigin={{ vertical: 'top', horizontal: 'center' }}
    {...props}
  >
    {props.children}
  </MuiMenu>
);

Menu.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.arrayOf(PropTypes.node),
  ]),
};
