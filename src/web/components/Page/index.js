import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import { CLASS_PAGE } from '../../css-api';

const styles = {
  root: {
    flexGrow: 1,
  },
};

const Component = React.forwardRef(({ id, classes, alignSelf, classNames, children }, ref) => (
  <div id={id} className={cx(classes.root, CLASS_PAGE, classNames)} style={{ alignSelf }} ref={ref}>
    {children}
  </div>
));

Component.propTypes = {
  id: PropTypes.string.isRequired,
  alignSelf: PropTypes.string,
  classNames: PropTypes.arrayOf(PropTypes.string),
  children: PropTypes.oneOfType([PropTypes.array, PropTypes.element]),
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Component);
