import React from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import { Logo } from '@sis-cc/dotstatsuite-visions';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import { fromSearchToState } from '../utils/router';
import { FormattedMessage } from '../i18n';
import { getAsset } from '../lib/settings';
import Page from './Page';
import { ID_ERROR_PAGE } from '../css-api';

const styles = theme => ({
  page: {
    backgroundColor: theme.palette.primary.main,
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
    paddingTop: theme.spacing(8),
    minHeight: '100vh',
  },
});

class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError(/*error*/) {
    // Update state so the next render will show the fallback UI.
    return { hasError: true };
  }

  componentDidCatch(error, errorInfo) {
    console.error(error, errorInfo); // eslint-disable-line no-console
  }

  render() {
    if (this.state.hasError) {
      if (this.props.isFinal) {
        return <h1>Whoops, something went wrong on our end.</h1>;
      }

      // selector are not possible in this component
      const localeId = R.prop('locale', fromSearchToState(window.location.search));

      return (
        <Page id={ID_ERROR_PAGE} classes={{ root: this.props.classes.page }}>
          <Logo logo={getAsset('subheader', localeId)}>
            <Typography variant="h6" color="secondary">
              <FormattedMessage id="de.error.title" />
            </Typography>
          </Logo>
        </Page>
      );
    }

    return this.props.children;
  }
}

ErrorBoundary.propTypes = {
  children: PropTypes.oneOfType([PropTypes.array, PropTypes.element]),
  classes: PropTypes.object.isRequired,
  isFinal: PropTypes.bool,
};

export default withStyles(styles)(ErrorBoundary);
