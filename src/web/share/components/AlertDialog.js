import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  titleContainer: {
    padding: theme.spacing(1, 2),
  },
  title: {
    borderBottom: `solid 2px ${theme.palette.grey[700]}`,
  },
}));

const AlertDialog = ({
  Icon,
  customButton,
  title,
  description,
  handleOpen,
  handleClose,
  open,
  children,
}) => {
  const classes = useStyles();
  const button = customButton || (
    <IconButton color="primary" aria-label="delete" size="small" onClick={handleOpen}>
      <Icon />
    </IconButton>
  );
  return (
    <Fragment>
      {button}
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title" className={classes.titleContainer} disableTypography>
          <Typography className={classes.title} component="h2" variant="h6">
            {title}
          </Typography>
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">{description}</DialogContentText>
        </DialogContent>
        <DialogActions>{children}</DialogActions>
      </Dialog>
    </Fragment>
  );
};

AlertDialog.propTypes = {
  Icon: PropTypes.object,
  labels: PropTypes.object,
  open: PropTypes.bool,
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
  customButton: PropTypes.node,
  title: PropTypes.oneOfType([PropTypes.element, PropTypes.string]),
  description: PropTypes.oneOfType([PropTypes.element, PropTypes.string]),
  handleOpen: PropTypes.func,
  handleClose: PropTypes.func,
};

export default AlertDialog;
