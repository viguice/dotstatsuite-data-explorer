import * as R from 'ramda';
import axios from 'axios';
import { rules2 } from '@sis-cc/dotstatsuite-components';

const axiosDefaults = {
  //transitional: { silentJSONParsing: false, forcedJSONParsing: false },
};

const _get = ({ requestArgs = {} /*parserArgs = {}*/ }) => {
  const { url, headers, params, responseType } = requestArgs;
  return axios.get(url, {
    ...axiosDefaults,
    responseType: R.isNil(responseType) ? 'json' : responseType,
    headers,
    params,
  });
};

const _post = ({ requestArgs = {}, callback }) => {
  const { url, headers, params, body } = requestArgs;
  return axios
    .post(url, body, { ...axiosDefaults, headers, params, body, mode: 'cors' })
    .then(res => {
      if (R.is(Function, callback)) {
        callback();
      }
      return res;
    });
};

const requestData = ({ requestArgs = {}, tooLongRequestErrorCallback }) => {
  const { url, datasourceId } = requestArgs;
  const configSpaces = window.CONFIG?.member?.scope?.spaces;
  const {
    supportsPostLongRequests = false,
    hasCustomRangeHeader = true,
  } = R.propOr({}, datasourceId, configSpaces);

  const _requestArgs = R.over(R.lensProp('headers'), headers => {
    const range = R.prop('x-range', headers);
    if (R.isNil(range) || hasCustomRangeHeader) {
      return headers;
    }
    return R.pipe(R.assoc('range', range), R.dissoc('x-range'))(headers);
  })(requestArgs);

  return _get({ requestArgs: _requestArgs }).catch(error => {
    if (error.request && !error.response) {
      //here we assume 414 case where url is too long even we cannot be sure
      if (supportsPostLongRequests) {
        const parsed = R.split('/', url);
        const dataquery = R.last(parsed);
        const newUrl = R.pipe(
          R.dropLast(1),
          R.append('body'),
          R.join('/'),
        )(parsed);
        const body = new FormData();
        body.append('key', dataquery);
        return _post({
          requestArgs: { ..._requestArgs, body, url: newUrl },
          callback: tooLongRequestErrorCallback,
        });
      }
    }
    throw error;
  });
};

const getStructure = args => _get(args).then(R.prop('data'));
const getAvailableConstraints = args => requestData(args).then(R.prop('data'));

const getData = args => {
  return requestData(args).then(({ data, headers }) => {
    const isSdmx3 = R.startsWith(
      rules2.SDMX_3_0_JSON_DATA_FORMAT,
      args?.requestArgs?.headers?.Accept,
    );
    const dataFunctor = isSdmx3 ? rules2.sdmx_3_0_DataFormatPatch : R.identity;
    return { data: dataFunctor(data), headers };
  });
};

const getMetadata = args => _get(args);
const getDataFile = args => requestData(args);

const methods = {
  getStructure,
  getStructureExternalResources: getStructure,
  getRelatedArtefact: getStructure,
  getHierarchicalCodelist: getStructure,
  getData,
  getMicrodata: getData,
  getBlankData: getData,
  getAvailableConstraints,
  getMetadata,
  getDataFile,
};

const error = method => () => {
  throw new Error(`Unkown method: ${method}`);
};

const main = ({ method, ...rest }) => (methods[method] || error(method))(rest);
R.compose(
  R.forEach(([name, fn]) => (main[name] = fn)),
  R.toPairs,
)(methods);

export default main;
