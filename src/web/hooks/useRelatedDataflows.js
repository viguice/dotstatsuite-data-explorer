import * as R from 'ramda';
import { useSelector } from 'react-redux';
import useSdmxQuery from './useSdmxQuery';
import {
  getRequestArgs,
  parseArtefactRelatives,
} from '@sis-cc/dotstatsuite-sdmxjs';
import { getDataflow, getLocale } from '../selectors/router';
import { getDatasource } from '../selectors/sdmx';
import useSdmxStructure from './useSdmxStructure';
import { getIdentifiers } from '../lib/sdmx/accessors/structure';
import { useMemo } from 'react';
import searchApi from '../api/search';
import { useQueries } from 'react-query';
import { getDatasourceIds } from '../lib/settings';

const filterIsIndexed = isIndexedDataflows =>
  R.addIndex(R.filter)((_, index) => {
    return R.path([index, 'data', 'searchResultNb'], isIndexedDataflows) === 1;
  });

export const useRelatedDataflows = () => {
  const locale = useSelector(getLocale);
  const { dataflowId, agencyId, version, datasourceId } = useSelector(
    getDataflow,
  );
  // datasourceId can be a datasourceId if coming to viz from search
  // but it can be a spaceId if coming from DLM!
  // hard to refactor because urls needs to change:
  // - ds for DE
  // - space for DLM
  // a space can be referenced in multiple datasources,
  // hence 'datasourceIds'
  const datasourceIds = getDatasourceIds(datasourceId);
  const datasource = useSelector(getDatasource);
  const { data: structure } = useSdmxStructure();
  const identifiers = getIdentifiers(structure);
  const hasIdentifiers = R.pipe(R.values, R.none(R.isNil))(identifiers);

  const requestArgs = getRequestArgs({
    identifiers,
    datasource,
    type: 'datastructure',
    withPartialReferences: false,
    params: {
      references: 'parents',
      detail: 'allcompletestubs',
    },
    locale,
  });

  const ctx = { method: 'getRelatedArtefact', requestArgs };
  const { data: relatedArtefacts } = useSdmxQuery(ctx, {
    isEnabled: hasIdentifiers,
  });

  const relatedDataflows = useMemo(() => {
    if (R.isNil(relatedArtefacts)) return [];
    const isDataflow = R.propEq('type', 'dataflow');
    const artefacts = parseArtefactRelatives({
      sdmxJson: relatedArtefacts,
      artefactId: `dataflow:${agencyId}:${dataflowId}(${version})`,
    });
    return R.filter(isDataflow, artefacts);
  }, [relatedArtefacts]);

  const searchQueries = useMemo(() => {
    return R.map(({ code, version, agencyId }) => {
      //eslint-disable-next-line
      const search = `dataflowId:\"${code}\" version:\"${version}\" agencyId:\"${agencyId}\"`;
      const ctx = {
        method: 'getSearch',
        requestArgs: {
          lang: locale,
          facets: { datasourceId: datasourceIds },
          fl: ['dataflowId'],
          rows: 0,
          search,
        },
      };
      return {
        queryKey: `@@search/${ctx.method} --> ${search}`,
        queryFn: () => searchApi(ctx),
      };
    }, relatedDataflows);
  }, [relatedDataflows]);

  const isIndexedDataflows = useQueries(searchQueries);

  const relatedIndexedDataflows = useMemo(() => {
    const areAllFetched = R.pipe(
      R.pluck('isFetched'),
      R.all(R.identity),
    )(isIndexedDataflows);
    // avoid blinking: if related is 4 but indexed is 2
    // display will show 4 then 2 when isIndexedDataflows request will be done
    if (!areAllFetched) return [];

    const relatedIndexedDataflows = filterIsIndexed(isIndexedDataflows)(
      relatedDataflows,
    );
    // if none of the related dataflows are indexed, return all related dataflows
    return R.isEmpty(relatedIndexedDataflows)
      ? relatedDataflows
      : relatedIndexedDataflows;
  }, [isIndexedDataflows, relatedDataflows]);

  return relatedIndexedDataflows;
};
