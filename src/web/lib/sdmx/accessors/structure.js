import * as R from 'ramda';
import { renameKeys } from '../../../utils';

/*
 * structure accessors should always and only take a valid SDMX structure as param
 * basic format of an accesor is: structure => {}
 * if a context is needed, use a fof: ctx => structure => {}
 */

const DFs = 'dataflows';
const DSDs = 'dataStructures';

const getHeadDataByKey = key => R.pathOr({}, ['data', key, 0]);
export const getDataStructure = getHeadDataByKey(DSDs);
export const getDataflow = getHeadDataByKey(DFs);
const getAnnotationsByKey = key =>
  R.pipe(getHeadDataByKey(key), R.propOr([], 'annotations'));
export const getDataStructureAnnotations = getAnnotationsByKey(DSDs);
export const getDataflowAnnotations = getAnnotationsByKey(DFs);
export const getIdentifiers = R.pipe(
  getDataStructure,
  R.pickAll(['agencyID', 'id', 'version']),
  renameKeys({ id: 'code', agencyID: 'agencyId' }),
);
