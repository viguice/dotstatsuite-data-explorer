import { createSelector } from 'reselect';
import { rules2 } from '@sis-cc/dotstatsuite-components';
import { getDataRequestRange } from './sdmx';
import {
  getMetadataCoordinates,
  getDuplicatedObservations,
  getHierarchisedDimensions,
  getOneValueDimensions,
  getAttributesSeries,
  getRefinedAttributes,
  getSeriesCombinations,
  getHeaderCoordinates,
  getManyValuesDimensions,
} from './data';
import {
  getCustomAttributes,
  getIsTimeInverted,
  getTableLayout,
} from './index';
import * as Layout from '../lib/layout';

export const getLayoutIds = createSelector(
  getTableLayout,
  getManyValuesDimensions,
  (layoutIds, dimensions) => Layout.getLayout(layoutIds, dimensions),
);

export const getLayout = createSelector(
  getLayoutIds,
  getHierarchisedDimensions,
  getSeriesCombinations,
  getIsTimeInverted,
  getOneValueDimensions,
  (layoutIds, dimensions, combinations, isTimeInverted, fixedDimensions) =>
    rules2.getLayout(
      layoutIds,
      dimensions,
      combinations,
      isTimeInverted,
      fixedDimensions,
    ),
);

export const getConfigLayout = createSelector(
  getLayout,
  Layout.getTableConfigLayout,
);

export const getSortedLayoutIndexes = createSelector(
  getLayout,
  getDuplicatedObservations,
  (layout, observations) => rules2.getSortedLayoutIndexes(layout, observations),
);

export const getRefinedLayoutIndexes = createSelector(
  getSortedLayoutIndexes,
  getLayout,
  getDuplicatedObservations,
  getDataRequestRange,
  (layoutIndexes, layout, observations, limit) => {
    const enhancedLayoutIndexes = rules2.parseLayoutIndexesHierarchies(
      layoutIndexes,
      layout,
    );
    return rules2.refineLayoutSize({ layout, observations, limit })(
      enhancedLayoutIndexes,
    );
  },
);

export const getLayoutData = createSelector(
  getRefinedLayoutIndexes,
  getLayout,
  getMetadataCoordinates,
  getAttributesSeries,
  getCustomAttributes,
  getHeaderCoordinates,
  (
    indexes,
    layout,
    metadataCoordinates,
    attributesSeries,
    customAttributes,
    topCoordinates,
  ) =>
    rules2.getLayoutData(indexes, layout, {
      metadataCoordinates,
      attributesSeries,
      customAttributes,
      topCoordinates,
    }),
);

export const getCellsAttributesIds = createSelector(
  getLayoutIds,
  getRefinedAttributes,
  (layoutIds, attributes) =>
    rules2.getCellsAttributesIds(layoutIds, attributes),
);

export const getIndexedCombinationsByDisplay = createSelector(
  getLayout,
  getSeriesCombinations,
  (layout, combinations) =>
    rules2.getIndexedCombinationsByDisplay(layout, combinations),
);

export const getCellsMetadataCoordinates = createSelector(
  getMetadataCoordinates,
  getOneValueDimensions,
  getLayoutIds,
  (metadataCoordinates, oneValueDimensions, layoutIds) =>
    rules2.getCellsMetadataCoordinates(
      metadataCoordinates,
      oneValueDimensions,
      layoutIds,
    ),
);

export const getCells = createSelector(
  getCustomAttributes,
  getCellsAttributesIds,
  getIndexedCombinationsByDisplay,
  getAttributesSeries,
  getCellsMetadataCoordinates,
  getDuplicatedObservations,
  (cA, cAI, comb, attrSeries, metCoord, obs) =>
    rules2.getCells(cA, cAI, comb, attrSeries, metCoord)(obs),
);

export const getCuratedCells = createSelector(
  getCells,
  getLayout,
  (cells, layout) => rules2.getCuratedCells(cells, layout),
);
