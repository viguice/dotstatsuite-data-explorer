import { createSelector } from 'reselect';
import * as R from 'ramda';
import { rules, rules2 } from '@sis-cc/dotstatsuite-components';
import { getDataflow, getDisplay, getLocale, getDataquery } from './router';
import { getData, getDataflowName, getHierarchies, getAnnotations } from './sdmx';
import { getDimensionId as getMicrodataDimensionId } from './microdata';
import { getCustomAttributes } from './index';
import * as metadataLib from '../lib/sdmx/metadata';
import { getDefaultCombinations } from '../lib/settings';

export const getMetadataCoordinates = createSelector(getData, data =>
  rules2.getMetadataCoordinates({ data }),
);

export const getDataAttributes = createSelector(
  getData,
  R.pathOr([], ['structure', 'attributes', 'observation']),
);

export const getDataDimensions = createSelector(
  getData,
  R.pathOr([], ['structure', 'dimensions', 'observation']),
);

export const getRefinedDimensions = createSelector(
  getDataDimensions,
  getDataquery,
  (dimensions, dataquery) => rules2.refineDimensions(dimensions, dataquery),
);

export const getCombinationDefinitions = createSelector(
  getAnnotations,
  getLocale,
  (annotations, locale) => {
    const dataflowCombs = rules2.getCombinationDefinitions(annotations, locale);
    if (!R.isEmpty(dataflowCombs)) {
      return dataflowCombs;
    }
    return getDefaultCombinations(locale);
  },
);

export const getParsedAttributes = createSelector(
  getDataAttributes,
  getRefinedDimensions,
  getCustomAttributes,
  (attributes, dimensions, customAttributes) =>
    rules2.parseAttributes(attributes, dimensions, customAttributes),
);

export const getOneValueDimensions = createSelector(
  getRefinedDimensions,
  getParsedAttributes,
  getCustomAttributes,
  (dimensions, attributes) => rules2.getOneValueDimensions(dimensions, attributes),
);

export const getHeaderCoordinates = createSelector(getOneValueDimensions, oneValueDimensions =>
  rules2.getHeaderCoordinates(oneValueDimensions),
);

export const getParsedCombinations = createSelector(
  getCombinationDefinitions,
  getParsedAttributes,
  getRefinedDimensions,
  (combinations, parsedAttributes, dimensions) =>
    rules2.parseCombinations(combinations, parsedAttributes, dimensions),
);

export const getSeriesCombinations = createSelector(
  getParsedCombinations,
  getOneValueDimensions,
  (combinations, oneValueDimensions) =>
    rules2.getSeriesCombinations(combinations, oneValueDimensions),
);

export const getRefinedAttributes = createSelector(
  getParsedAttributes,
  getSeriesCombinations,
  (parsedAttributes, seriesCombinations) =>
    rules2.refineAttributes(parsedAttributes, seriesCombinations),
);

export const getDataObservations = createSelector(getData, data => rules.getObservations({ data }));

export const getEnhancedObservations = createSelector(
  getDataObservations,
  getRefinedAttributes,
  getDataDimensions,
  getCustomAttributes,
  (observations, attributes, dimensions, customAttributes) =>
    rules2.enhanceObservations(dimensions, observations, attributes, { customAttributes }),
);

export const getAttributesSeries = createSelector(getEnhancedObservations, observations =>
  rules2.getAttributesSeries(observations),
);

export const getSeriesAdvancedAttributes = createSelector(
  getAttributesSeries,
  getParsedCombinations,
  getCustomAttributes,
  (attributesSeries, combinations, customAttributes) =>
    metadataLib.getSeriesAdvancedAttributes(attributesSeries, combinations, customAttributes),
);

export const getHeaderAdvancedAttributes = createSelector(
  getRefinedAttributes,
  getDataDimensions,
  getParsedCombinations,
  getCustomAttributes,
  (attributes, dimensions, combinations, customAttributes) =>
    metadataLib.getHeaderAdvancedAttributes(attributes, dimensions, combinations, customAttributes),
);

export const getAdvancedAttributes = createSelector(
  getSeriesAdvancedAttributes,
  getHeaderAdvancedAttributes,
  (seriesAttrs, headerAttrs) => ({ ...seriesAttrs, ...headerAttrs }),
);

export const getManyValuesDimensions = createSelector(
  getRefinedDimensions,
  getAttributesSeries,
  getCustomAttributes,
  getSeriesCombinations,
  (dimensions, attributesSeries, customAttributes, combinations) =>
    rules2.getManyValuesDimensions(dimensions, attributesSeries, customAttributes, combinations),
);

export const getHierarchisedDimensions = createSelector(
  getManyValuesDimensions,
  getHierarchies,
  (dimensions, hierarchies) =>
    R.map(dim => {
      if (R.isEmpty(R.propOr({}, dim.id, hierarchies))) {
        return rules2.hierarchiseDimensionWithNativeHierarchy(dim);
      }
      return rules2.hierarchiseDimensionWithAdvancedHierarchy(dim, R.prop(dim.id, hierarchies));
    }, dimensions),
);

export const getDuplicatedObservations = createSelector(
  getEnhancedObservations,
  getHierarchisedDimensions,
  (observations, hierarchisedDimensions) =>
    rules2.duplicateObs(R.values(hierarchisedDimensions), observations),
);

export const getHeaderCombinations = createSelector(
  getParsedCombinations,
  getOneValueDimensions,
  getRefinedAttributes,
  getDisplay,
  (combinations, dimensions, attributes, display) =>
    rules2.getHeaderCombinations(combinations, dimensions, attributes, display),
);

export const getVisDataflow = createSelector(getDataflow, getDataflowName, (dataflow, name) => ({
  id: dataflow.dataflowId,
  name,
}));

export const getDefaultTitleLabel = createSelector(
  getVisDataflow,
  getDisplay,
  (dataflow, display) => rules.dimensionValueDisplay(display)(dataflow),
);

export const getDataflowAttributes = createSelector(
  getParsedAttributes,
  getParsedCombinations,
  (attributes, combinations) => rules2.getDataflowAttributes(attributes, combinations),
);

export const getHeaderTitle = createSelector(
  getVisDataflow,
  getDataflowAttributes,
  getDisplay,
  getCustomAttributes,
  (dataflow, attributes, display, customAttributes) =>
    rules2.getHeaderTitle(dataflow, attributes, display, customAttributes),
);

export const getHeaderSubtitle = createSelector(
  getOneValueDimensions,
  getParsedCombinations,
  getCustomAttributes,
  getDisplay,
  getMicrodataDimensionId,
  (dimensions, combinations, customAttributes, display, microdataDimension) =>
    rules2.getHeaderSubtitle(
      dimensions,
      combinations,
      customAttributes,
      display,
      microdataDimension,
    ),
);

export const getHeaderProps = createSelector(
  getHeaderTitle,
  getHeaderSubtitle,
  getHeaderCombinations,
  (title, subtitle, combinations) => ({
    title,
    subtitle,
    combinations,
  }),
);
