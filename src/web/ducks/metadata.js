import { CHANGE_DATAFLOW, RESET_DATAFLOW, FLUSH_DATA } from './sdmx';

const model = () => ({
  coordinates: undefined,
  error: undefined,
  metadataSeries: undefined,
  msd: undefined,
  isLoading: false,
  isDownloading: false,
  isOpen: false,
});

export const METADATA = 'metadata';

export const TOGGLE_METADATA = '@@metadata/toggle';
export const REQUEST_METADATA = '@@metadata/request';
export const REQUEST_METADATA_ERROR = '@@metadata/request_error';
export const HANDLE_METADATA = '@@metadata/handle';
export const DOWNLOAD_METADATA = '@@metadata/download';
export const DOWNLOAD_METADATA_SUCCESS = '@@metadata/download_success';
export const FLUSH_METADATA = '@@metadata/flush';
export const HANDLE_MSD = '@@metadata/handle_msd';

export const toggleMetadata = payload => ({
  type: TOGGLE_METADATA,
  payload,
});

export const requestMetadata = () => ({
  type: REQUEST_METADATA,
});

export const handleMetadata = metadataSeries => ({
  type: HANDLE_METADATA,
  metadataSeries,
});

export const downloadMetadata = () => ({
  type: DOWNLOAD_METADATA,
});

export const flushMetadata = () => ({
  type: FLUSH_METADATA,
});

export default (state = model(), action = {}) => {
  switch (action.type) {
    case TOGGLE_METADATA:
      return { ...state, isOpen: true, coordinates: action.payload.coordinates };
    case REQUEST_METADATA:
      return { ...state, isLoading: true };
    case REQUEST_METADATA_ERROR:
      return { ...state, isLoading: false, error: action.payload.error };
    case HANDLE_METADATA:
      return { ...state, isLoading: false, metadataSeries: action.metadataSeries };
    case DOWNLOAD_METADATA:
      return { ...state, isDownloading: true };
    case DOWNLOAD_METADATA_SUCCESS:
      return { ...state, isDownloading: false };
    case HANDLE_MSD:
      return { ...state, msd: action.payload.msd };
    case FLUSH_METADATA:
      return { ...model(), msd: state.msd };
    case RESET_DATAFLOW:
    case CHANGE_DATAFLOW:
    case FLUSH_DATA:
      return model();
    default:
      return state;
  }
};
