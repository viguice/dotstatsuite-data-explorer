import * as R from 'ramda';
import { updateDataquery, getDataquery } from '@sis-cc/dotstatsuite-sdmxjs';
import {
  getDataquery as getRouterDataquery,
  getPeriod,
  getLastNMode,
} from '../selectors/router';
import {
  getDimensions,
  getFrequency,
  getFrequencyArtefact,
  getUsedFilters,
} from '../selectors/sdmx';
import {
  getSelectedIdsIndexed,
  setSelectedDimensionsValues,
  getOnlyHasDataDimensions,
} from '../lib/sdmx';
import {
  getSdmxPeriod,
  changeFrequency,
  getFrequencies,
} from '../lib/sdmx/frequency';
import {
  START_PERIOD,
  END_PERIOD,
  LASTNOBSERVATIONS,
  LASTNPERIODS,
} from '../utils/used-filter';
import { getHighlightedConstraints } from '../utils';

//---------------------------------------------------------------------------------------------model
export const model = () => ({
  dataRequestSize: null,
  datasetAttributes: null,
  attributes: [],
  constraints: { availableWithoutPeriodFetched: false },
  dimensions: [],
  data: undefined,
  layout: {},
  frequencyArtefact: {},
  externalResources: [],
  hiddenValuesAnnotation: {},
  hierarchySchemes: [],
  timePeriodArtefact: undefined,
  range: {},
  observationsCount: undefined,
  observationsType: undefined,
  externalReference: undefined,
  name: undefined,
  hierarchies: {},
  isDataUrlTooLong: false,
  defaultPeriod: undefined,
  isApiQueryCopied: false,
  textAlign: undefined,
});

//-----------------------------------------------------------------------------------------constants
export const HANDLE_STRUCTURE = '@@sdmx/HANDLE_STRUCTURE';
export const REQUEST_STRUCTURE = '@@sdmx/REQUEST_STRUCTURE';
export const PARSE_STRUCTURE = '@@sdmx/PARSE_STRUCTURE';
export const HANDLE_DATA = '@@sdmx/HANDLE_DATA';
export const REQUEST_DATA = '@@sdmx/REQUEST_DATA';
export const PARSE_DATA = '@@sdmx/PARSE_DATA';
export const FLUSH_DATA = '@@sdmx/FLUSH_DATA';
export const RESET_SDMX = '@@sdmx/RESET_SDMX';
export const REQUEST_VIS_DATAFILE = '@@sdmx/REQUEST_VIS_DATAFILE';
export const REQUEST_SEARCH_DATAFILE = '@@sdmx/REQUEST_SEARCH_DATAFILE';
export const CHANGE_DATAFLOW = '@@sdmx/CHANGE_DATAFLOW';
export const RESET_DATAFLOW = '@@sdmx/RESET_DATAFLOW';
export const CHANGE_DATAQUERY = '@@sdmx/CHANGE_DATAQUERY';
export const APPLY_DATA_AVAILABILITY = '@@sdmx/APPLY_DATA_AVAILABILITY';
export const CHANGE_FREQUENCY_PERIOD = '@@sdmx/CHANGE_FREQUENCY_PERIOD';
export const CHANGE_LAST_N_OBS = '@@sdmx/CHANGE_LAST_N_OBS';
export const RESET_SPECIAL_FILTERS = '@@sdmx/RESET_SPECIAL_FILTERS';
export const EXTERNAL_REFERENCE = '@@sdmx/EXTERNAL_REFERENCE';
export const HANDLE_EXTERNAL_RESOURCES = '@@sdmx/HANDLE_EXTERNAL_RESOURCES';
export const REQUEST_EXTERNAL_RESOURCES = '@@search/REQUEST_EXTERNAL_RESOURCES';
export const HANDLE_AVAILABLE_CONSTRAINTS =
  '@@sdmx/HANDLE_AVAILABLE_CONSTRAINTS';
export const IS_DATA_URL_TOO_LONG = `@@sdmx/IS_DATA_URL_TOO_LONG`;
export const COPY_API_QUERY = `@@sdmx/COPY_API_QUERY`;
export const CHANGE_FREQUENCY_PERIOD_DATAQUERY =
  '@@sdmx/CHANGE_FREQUENCY_PERIOD_DATAQUERY';
export const REQUEST_DATA_PROPS = ['dataquery', 'lastNObservations', 'period'];
export const REQUEST_ALL_HCL = '@@sdmx/REQUEST_ALL_HCL';
export const HANDLE_HCLS = '@@sdmx/HANDLE_HCLS';

//------------------------------------------------------------------------------------------creators
export const requestData = ({ shouldRequestStructure } = {}) => ({
  type: REQUEST_DATA,
  shouldRequestStructure,
});

export const requestVisDataFile = ({ isDownloadAllData }) => ({
  type: REQUEST_VIS_DATAFILE,
  payload: { isDownloadAllData },
});

export const requestSearchDataFile = ({ dataflow }) => ({
  type: REQUEST_SEARCH_DATAFILE,
  payload: { dataflow },
});

export const requestExternalResources = ({ dataflow }) => ({
  type: REQUEST_EXTERNAL_RESOURCES,
  dataflow,
});

export const resetSdmx = () => ({
  type: RESET_SDMX,
  request: 'getSearch',
});

export const dataUrlIsTooLong = () => ({ type: IS_DATA_URL_TOO_LONG });
export const copyApiQuery = () => ({ type: COPY_API_QUERY });

// actionHistory could be replaceHistory, pushHistory.
export const changeDataflow = (
  dataflow,
  actionHistory = 'pushHistory',
  viewer,
) => dispatch => {
  if (R.isNil(dataflow)) return;
  dispatch({
    type: CHANGE_DATAFLOW,
    [actionHistory]: {
      pathname: '/vis',
      payload: {
        viewer,
        dataflow,
        highlightedConstraints: getHighlightedConstraints(
          R.propOr([], 'highlights', dataflow),
        ),
      },
    },
  });
};

export const resetDataflow = () => dispatch => {
  dispatch(resetSdmx());
  dispatch({
    type: RESET_DATAFLOW,
    pushHistory: {
      pathname: '/',
      payload: {
        dataflow: null,
        filter: null,
        dataquery: null,
        hasDataAvailability: null,
        viewer: null,
        // period: [undefined, undefined] is used to force no period, null means that default can be used
        period: null,
        lastNObservations: null,
        lastNMode: null,
        layout: null,
        time: null,
        microdataConstraints: null,
        highlightedConstraints: null,
        display: null,
        hierarchies: {},
      },
    },
  });
};

export const changeLastNObservations = (
  lastNObservations,
  lastNMode,
) => dispatch => {
  let payload = { lastNMode, lastNObservations };
  if (R.equals(lastNMode, LASTNPERIODS)) {
    payload.period = null;
  }
  if (R.isNil(lastNObservations) || R.isEmpty(lastNObservations)) {
    payload.lastNMode = undefined;
    payload.period = [undefined, undefined];
  }
  if (
    !R.isNil(lastNObservations) &&
    !R.isEmpty(lastNObservations) &&
    R.isNil(lastNMode)
  ) {
    payload.lastNMode = LASTNPERIODS;
    payload.period = null;
  }
  dispatch({
    type: CHANGE_LAST_N_OBS,
    pushHistory: { pathname: '/vis', payload },
    request: 'getData',
  });
};

export const resetFilters = () => (dispatch, getState) => {
  const selection = getUsedFilters(getState());
  const selectedConstraints = R.reduce(
    (acc, dim) => R.assoc(dim.id, R.map(R.path([0, 'id']), dim.values), acc),
    {},
    selection,
  );
  const updatedQuery = updateDataquery(
    getDimensions(getState()),
    getRouterDataquery(getState()),
    selectedConstraints,
  );
  dispatch({
    type: CHANGE_DATAQUERY,
    pushHistory: {
      pathname: '/vis',
      payload: {
        dataquery: updatedQuery,
        period: [undefined, undefined],
        lastNObservations: null,
        lastNMode: null,
      },
    },
    request: 'getData',
  });
};

export const resetSpecialFilters = (_, ids) => (dispatch, getState) => {
  const id = R.is(Array)(ids) ? R.head(ids) : ids;
  if (R.equals(LASTNOBSERVATIONS, id))
    return dispatch(changeLastNObservations());

  // special is start OR end (period)
  if (R.either(R.equals(START_PERIOD), R.equals(END_PERIOD))(id)) {
    const payloadPeriod = period => {
      if (R.equals(START_PERIOD, id)) return [undefined, R.last(period)];
      if (R.equals(END_PERIOD, id)) return [R.head(period), undefined];
    };
    return dispatch({
      type: CHANGE_FREQUENCY_PERIOD,
      request: 'getData',
      pushHistory: {
        pathname: '/vis',
        payload: { period: payloadPeriod(getPeriod(getState())) },
      },
    });
  }

  // reset all special filters (period, lastNObservations)
  dispatch({
    type: RESET_SPECIAL_FILTERS,
    pushHistory: {
      pathname: '/vis',
      payload: {
        period: [undefined, undefined],
        lastNObservations: null,
        lastNMode: null,
      },
    },
    request: 'getData',
  });
};

export const changeDataquery = (filterId, valueIds) => (dispatch, getState) => {
  const dataquery = updateDataquery(
    getDimensions(getState()),
    getRouterDataquery(getState()),
    {
      [filterId]: valueIds,
    },
  );
  dispatch({
    type: CHANGE_DATAQUERY,
    pushHistory: { pathname: '/vis', payload: { dataquery } },
    request: 'getData',
  });
};

export const applyDataAvailability = hasDataAvailability => (
  dispatch,
  getState,
) => {
  dispatch({
    type: APPLY_DATA_AVAILABILITY,
    pushHistory: { pathname: '/vis', payload: { hasDataAvailability } },
  });

  if (R.not(hasDataAvailability)) return;

  const currentDataquery = getRouterDataquery(getState());
  const dimensions = getDimensions(getState());
  const dimensionsWithSelectedValues = setSelectedDimensionsValues(
    currentDataquery,
    dimensions,
  );
  const frequencyArtefact = getFrequencyArtefact(getState());
  const frequencyId = R.prop('id')(frequencyArtefact);
  const availableFrequencies = R.pipe(
    getOnlyHasDataDimensions,
    R.head,
    getFrequencies,
  )([frequencyArtefact]);
  const frequency = changeFrequency(getFrequency(getState()))(
    availableFrequencies,
  );
  const selection = R.assoc(
    frequencyId,
    frequency,
    getSelectedIdsIndexed(dimensionsWithSelectedValues),
  );
  const dataquery = getDataquery(dimensions, selection);

  if (R.equals(currentDataquery, dataquery)) return;

  dispatch({ type: CHANGE_FREQUENCY_PERIOD, payload: { frequency } });
  dispatch({
    type: CHANGE_DATAQUERY,
    pushHistory: { pathname: '/vis', payload: { dataquery } },
    request: 'getData',
  });
};

export const changeFrequencyPeriod = ({ valueId, period } = {}) => (
  dispatch,
  getState,
) => {
  const filterId = R.prop('id')(getFrequencyArtefact(getState()));
  const frequency = getFrequency(getState());
  const lastNMode = getLastNMode(getState());
  const nextPeriod =
    lastNMode === LASTNPERIODS ? null : R.map(getSdmxPeriod(valueId))(period);
  const payload = {
    period: nextPeriod,
  };
  const action = {
    type: CHANGE_FREQUENCY_PERIOD,
    pushHistory: {
      pathname: '/vis',
      payload,
    },
  };

  if (R.not(R.isNil(filterId)) && frequency !== valueId) {
    const dataquery = updateDataquery(
      getDimensions(getState()),
      getRouterDataquery(getState()),
      {
        [filterId]: [valueId],
      },
    );
    dispatch({
      type: CHANGE_FREQUENCY_PERIOD_DATAQUERY,
      pushHistory: {
        pathname: '/vis',
        payload: { ...payload, dataquery },
      },
      request: 'getData',
    });
    return;
  }

  dispatch(R.assoc('request', 'getData', action));
};

//-------------------------------------------------------------------------------------------reducer
export default (state = model(), action = {}) => {
  switch (action.type) {
    case IS_DATA_URL_TOO_LONG:
      return R.set(R.lensProp('isDataUrlTooLong'), true, state);
    case FLUSH_DATA:
      return R.pipe(
        R.set(R.lensProp('data'), undefined),
        R.set(R.lensProp('range'), undefined),
      )(state);
    case EXTERNAL_REFERENCE:
      return R.set(
        R.lensProp('externalReference'),
        R.prop('externalReference', action),
      )(state);
    case HANDLE_HCLS:
      return R.set(
        R.lensProp('hierarchies'),
        R.prop('hierarchies', action),
      )(state);
    case HANDLE_STRUCTURE:
      return R.pipe(
        R.set(
          R.lensProp('automatedSelections'),
          R.path(['structure', 'automatedSelections'], action),
        ),
        R.set(
          R.lensProp('dataRequestSize'),
          R.path(['structure', 'dataRequestSize'], action),
        ),
        R.set(
          R.lensProp('externalResources'),
          R.path(['structure', 'externalResources'], action),
        ),
        R.set(
          R.lensProp('dimensions'),
          R.path(['structure', 'dimensions'], action),
        ),
        R.set(
          R.lensProp('attributes'),
          R.path(['structure', 'attributes'], action),
        ),
        R.set(
          R.lensProp('timePeriodArtefact'),
          R.path(['structure', 'timePeriodArtefact'], action),
        ),
        R.set(R.lensProp('name'), R.path(['structure', 'title'], action)),
        R.set(
          R.lensProp('description'),
          R.path(['structure', 'description'], action),
        ),
        R.set(
          R.lensProp('hierarchySchemes'),
          R.path(['structure', 'hierarchySchemes'], action),
        ),
        R.set(
          R.lensPath(['constraints', 'actual']),
          R.path(['structure', 'actualContentConstraints'], action),
        ),
        R.set(
          R.lensProp('observationsCount'),
          R.path(['structure', 'observationsCount'], action),
        ),
        R.set(
          R.lensProp('observationsType'),
          R.path(['structure', 'observationsType'], action),
        ),
        R.set(
          R.lensPath(['constraints', 'validFrom']),
          R.path(['structure', 'validFrom'], action),
        ),
        R.dissoc('range'),
        R.set(
          R.lensProp('hiddenValuesAnnotation'),
          R.path(['structure', 'hiddenValuesAnnotation'], action),
        ),
        R.set(
          R.lensProp('defaultPeriod'),
          R.path(['structure', 'defaultPeriod'], action),
        ),
        R.set(
          R.lensProp('textAlign'),
          R.path(['structure', 'textAlign'], action),
        ),
      )(state);
    case HANDLE_AVAILABLE_CONSTRAINTS:
      return R.pipe(
        R.set(
          R.lensPath(['constraints', 'available']),
          action.availableConstraints,
        ),
        R.set(R.lensPath(['constraints', 'dataPoints']), action.dataPoints),
        R.set(
          R.lensPath(['constraints', 'availableWithoutPeriodFetched']),
          true,
        ),
      )(state);
    case HANDLE_DATA:
      return R.pipe(
        R.set(R.lensProp('data'), R.prop('data', action)),
        R.set(R.lensProp('range'), R.prop('range', action)),
      )(state);
    case COPY_API_QUERY:
      return R.set(R.lensProp('isApiQueryCopied'), true, state);
    case RESET_SDMX:
      return { ...state, ...model() };
    default:
      return state;
  }
};
