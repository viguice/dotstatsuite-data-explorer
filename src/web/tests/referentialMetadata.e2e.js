import { changeVibe, testidSelector } from './utils';
import { length } from 'ramda';

describe('referential metadata: basic usecase', () => {
  let page;
  beforeAll(async done => {
    await changeVibe('nsi')('sna');
    page = await global.__BROWSER__.newPage();
    await page.setViewport({ width: 1200, height: 800 });
    const url = global._E2E_.makeUrl(
      `/vis?tenant=oecd:de&lc=en&df[ds]=hybrid&df[id]=SNA_TABLE1&df[ag]=OECD&df[vs]=1.0&av=true&pd=2019%2C2020&dq=AUS%2BAUT%2BBEL%2BCAN%2BCHL.B1G_P119.V%2BC`,
    );
    await page.goto(url);
    done();
  });
  it('load the viz', async () => {
    await page.waitForSelector(testidSelector('table-button'));
    const tableButton = await page.$(testidSelector('table-button'));
    await tableButton.click();
  });
  it('load the viz page with a table', async () => {
    await page.waitForSelector(testidSelector('vis-table'));
  });
  it('should find 4 annotations in the table', async () => {
    const annotations = await page.$$(testidSelector('ref-md-info'));
    expect(length(annotations)).toEqual(4);
    if (process.env.E2E_ENV === 'debug') await page.waitFor(250);
  });
  it('should click on the first annotation and open the metadata panel', async () => {
    const annotationHandle = await page.$(testidSelector('ref-md-info'));
    await annotationHandle.click();
    annotationHandle.dispose();
    if (process.env.E2E_ENV === 'debug') await page.waitFor(500);
  });
  it('should load and display the metadata', async () => {
    const titleHandle = await page.waitForSelector(
      `${testidSelector('ref-md-panel')} > div > h5`,
    );
    const text = await page.evaluate(el => el.textContent, titleHandle);
    expect(text).toContain('1. Gross domestic product (GDP)');
    await titleHandle.dispose();
  });
  it('should not display advanced attributes', async () => {
    const panel = await page.$(testidSelector('ref-md-panel'));
    const buttonTexts = await panel.$$eval('button', nodes =>
      nodes.map(n => n.textContent),
    );
    expect(buttonTexts).not.toContain('Data Characteristics');
  });
  it('should close the metadata panel', async () => {
    const closeHandle = await page.$(
      `${testidSelector('ref-md-panel')} > .MuiBackdrop-root`,
    );
    await closeHandle.click();
    await closeHandle.dispose();
  });
  it('should not see the panel anymore', async () => {
    const panelHandle = await page.waitForSelector(
      testidSelector('ref-md-panel'),
    );
    expect(panelHandle).not.toBe(null);
    await panelHandle.dispose();
  });
  it('should have metadata icon in section MEASURE=C', async () => {
    const section = await page.waitForSelector('th[id="MEASURE=C"]');
    const icon = await section.$(testidSelector('ref-md-info'));
    expect(icon).not.toBe(null);
  });
});
