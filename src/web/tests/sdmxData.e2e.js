import { changeVibe, testidSelector } from './utils';

describe('sdmx data: range header check without warning limitation disclaimer', () => {
  let page;
  beforeAll(async done => {
    await changeVibe('nsi')('sna');
    page = await global.__BROWSER__.newPage();
    await page.setViewport({ width: 1200, height: 800 });
    const url = global._E2E_.makeUrl(
      `/vis?tenant=oecd:de&lc=en&df[ds]=hybrid&df[id]=SNA_TABLE1&df[ag]=OECD&df[vs]=1.0&av=true&pd=2019%2C2020`,
    );
    await page.goto(url);
    done();
  });
  it('load the viz page and wait for the table to be displayed', async done => {
    await page.waitForSelector(testidSelector('vis-table'));
    done();
  });
  it('should not find the warning limitation disclaimer in the header', async done => {
    const disclaimer = await page.$(testidSelector('data-header-disclaimer'));
    expect(disclaimer).toBe(null);
    done();
  });
});

describe('sdmx data: range header check with warning limitation disclaimer', () => {
  let page;
  beforeAll(async done => {
    await changeVibe('config')('range');
    await changeVibe('nsi')('sna:content-range');
    page = await global.__BROWSER__.newPage();
    await page.setViewport({ width: 1200, height: 800 });
    const url = global._E2E_.makeUrl(
      `/vis?tenant=oecd:de&lc=en&df[ds]=hybrid&df[id]=SNA_TABLE1&df[ag]=OECD&df[vs]=1.0&av=true&pd=2019%2C2020`,
    );
    await page.goto(url);
    done();
  });
  it('load the viz page and wait for the table to be displayed', async done => {
    await page.waitForSelector(testidSelector('vis-table'));
    done();
  });
  it('should find the warning limitation disclaimer in the header', async done => {
    const disclaimer = await page.$(testidSelector('data-header-disclaimer'));
    expect(disclaimer).not.toBe(null);
    done();
  });
});
