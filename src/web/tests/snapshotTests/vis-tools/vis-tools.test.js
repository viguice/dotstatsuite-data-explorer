import React from 'react';
import Component from '../../../components/vis-tools';
import { render } from '../mockProviders';

describe('vis tools component', () => {
  it('should render with isFull flase', () => {
    const props = {
      maxWidth: 250,
      isFull: false,
      viewerProps: {},
      properties: {},
    };
    const { container } = render(<Component {...props} />);
    expect(container).toMatchSnapshot();
  });
  it('should render with isFull true', () => {
    const props = {
      maxWidth: 250,
      isFull: true,
      viewerProps: {},
      properties: {},
    };
    const { container } = render(<Component {...props} />);
    expect(container).toMatchSnapshot();
  });
  it('should render with isFull false', () => {
    const props = {
      maxWidth: 250,
      isFull: false,
      viewerProps: {},
      properties: {},
    };
    const { container } = render(<Component {...props} />);
    expect(container).toMatchSnapshot();
  });
});
