import { changeVibe, testidSelector, getTableLayout } from './utils';

describe('irregular time periods', () => {
  let page;
  beforeAll(async done => {
    await changeVibe('nsi')('irregular');
    page = await global.__BROWSER__.newPage();
    await page.setViewport({ width: 1200, height: 800 });
    const url = global._E2E_.makeUrl(
      `/vis?tenant=oecd:de&lc=fr&df[ds]=hybrid&df[id]=DF_KEI&df[ag]=OECD&df[vs]=1.0&dq=.ARG..A&pd=2004%2C2009&vw=tb`,
    );
    await page.goto(url);
    done();
  });
  it('load the viz page and wait for the table to be displayed', async () => {
    await page.waitForSelector(testidSelector('vis-table'));
    if (process.env.E2E_ENV === 'debug') await page.waitFor(250);
  });
  it('table layout', async () => {
    expect(await getTableLayout(page)).toEqual({
      sections: [],
      header: ['TIME_PERIOD'],
      rows: ['SUBJECT'],
    });
  });
  it('time periods header french labels', async () => {
    const labels = await page.$$eval('th[headers="header_0"]', nodes =>
      nodes.map(n => n.textContent),
    );
    expect(labels).toEqual([
      '2004',
      '2005',
      '2006 - 2007',
      '2006',
      'janv. - mars 2006',
      '2006-03-26T21:15:20.1000',
      '2007 - 2008',
      '2007',
      '2007-05-16 15:34:20 - 17:34:19',
      '2008',
      '2009',
    ]);
  });
  it('switch locale to english', async done => {
    await page.click('#languages');
    if (process.env.E2E_ENV === 'debug') await page.waitFor(1000);
    await page.click('li#en');
    done();
  });
  it('ascending order time periods header english labels', async () => {
    await page.waitForSelector(testidSelector('vis-table'));
    const labels = await page.$$eval('th[headers="header_0"]', nodes =>
      nodes.map(n => n.textContent),
    );
    expect(labels).toEqual([
      '2004',
      '2005',
      '2006 - 2007',
      '2006',
      '2006-Jan - 2006-Mar',
      '2006-03-26T21:15:20.1000',
      '2007 - 2008',
      '2007',
      '2007-05-16 15:34:20 - 17:34:19',
      '2008',
      '2009',
    ]);
  });
  it('switch time order to descending', async () => {
    const toolbar = await page.$(testidSelector('detoolbar'));
    const customizeButton = await toolbar.$(testidSelector('customize'));
    await customizeButton.click();
    await page.waitForSelector(testidSelector('table-layout-test-id'));
    const timePeriodDragItem = await page.$(testidSelector('draggable-TIME_PERIOD'));
    const timeOrderInput = await timePeriodDragItem.$('div[role="button"]');
    await timeOrderInput.click();
    const descendingValue = await page.$('li[data-value="descending"]');
    await page.waitForTimeout(500);
    await descendingValue.click();
    await page.waitForTimeout(1500);
  });
  it('ascending order time periods header english labels', async () => {
    await page.waitForSelector(testidSelector('vis-table'));
    const labels = await page.$$eval('th[headers="header_0"]', nodes =>
      nodes.map(n => n.textContent),
    );
    expect(labels).toEqual([
      '2009',
      '2008',
      '2007-05-16 15:34:20 - 17:34:19',
      '2007',
      '2007 - 2008',
      '2006-03-26T21:15:20.1000',
      '2006-Jan - 2006-Mar',
      '2006',
      '2006 - 2007',
      '2005',
      '2004',
    ]);
  });
  it('period in applied filters chips', async () => {
    const appliedValuesChips = await page.$$eval(testidSelector('deleteChip-test-id'), nodes =>
      nodes.map(n => n.ariaLabel),
    );
    expect(appliedValuesChips).toContain('Start: 2004');
    expect(appliedValuesChips).toContain('End: 2009');
  });
  it('open time period filter', async () => {
    const panel = await page.$('div[id="PANEL_PERIOD"]');
    await panel.click();
    await page.$(testidSelector('period-picker-test-id'));
  });
  it('change time period start', async () => {
    const startInput = await page.$(testidSelector('year-Start-test-id'));
    await startInput.click();
    await page.$('ul[aria-labelledby="year-Start-label"]');
  });
  it('select 2007 value', async () => {
    const startOptionsList = await page.$('ul[aria-labelledby="year-Start-label"]');
    const inputValue = await startOptionsList.$('li[id="2007"]');
    await page.waitForTimeout(500);
    await inputValue.click();
    await page.waitForTimeout(500);
  });
  it('period updated in url', async () => {
    const url = await page.url();
    expect(url).toContain('pd=2007%2C2009');
  });
  it('period updated in applied filters chips', async () => {
    const appliedValuesChips = await page.$$eval(testidSelector('deleteChip-test-id'), nodes =>
      nodes.map(n => n.ariaLabel),
    );
    expect(appliedValuesChips).toContain('Start: 2007');
    expect(appliedValuesChips).toContain('End: 2009');
  });
});
