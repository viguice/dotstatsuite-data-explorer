import { changeVibe, testidSelector } from '../utils';

describe('search single result to viz page', () => {
  let page;
  beforeAll(async done => {
    await changeVibe('sfs')('oecd');
    page = await global.__BROWSER__.newPage();
    await page.setViewport({ width: 1200, height: 800 });
    const url = global._E2E_.makeUrl(`?tenant=oecd:de`);
    await page.goto(url);
    done();
  });
  beforeEach(async () => {
    if (process.env.E2E_ENV === 'debug') await page.waitFor(1000);
  });
  it('should find datasources facet in home page', async done => {
    await page.waitForSelector('[id=id_home_page]');
    const facet = await page.$(testidSelector('collapseButton_datasourceId'));
    expect(facet).not.toEqual(null);
    done();
  });
  it('open datasources facet and select ds-demo-reset', async done => {
    await changeVibe('nsi')('oecd');
    const facet = await page.$(testidSelector('collapseButton_datasourceId'));
    await facet.click();
    await page.waitForSelector('[aria-label=ds-demo-reset]');
    const facetValue = await page.$('[aria-label=ds-demo-reset]');
    await facetValue.click();
    done();
  });
  it('should open directly visPage', async done => {
    await page.waitForSelector('#id_viewer_component');
    done();
  });
  it('should find go back to search link', async done => {
    const link = await page.$(testidSelector('back-search-link'));
    expect(link).not.toEqual(null);
    done();
  });
  it('clicking the link should go back to home page', async done => {
    const link = await page.$(testidSelector('back-search-link'));
    await link.click();
    await page.waitForSelector('[id=id_home_page]');
    done();
  });
  it('open a viz page without search params in url', async done => {
    await changeVibe('nsi')('ddown');
    const nextUrl = global._E2E_.makeUrl(
      `/vis?tenant=oecd:de&lc=en&df[ds]=hybrid&df[id]=DSD_DEBT_TRANS_DDOWN@DF_DDOWN&df[ag]=OECD.DAF&df[vs]=1.0&vw=tb`,
    );
    await page.goto(nextUrl);
    done();
  });
  it('should find go back to search link', async done => {
    const link = await page.$(testidSelector('back-search-link'));
    expect(link).not.toEqual(null);
    done();
  });
  it('clicking the link should go back to home page', async done => {
    const link = await page.$(testidSelector('back-search-link'));
    await link.click();
    await page.waitForSelector('[id=id_home_page]');
    done();
  });
});
