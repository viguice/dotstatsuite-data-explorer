import { changeVibe, testidSelector } from './utils';

describe('load indexed related (by DSD) dataflows', () => {
  let page;
  beforeAll(async done => {
    await changeVibe('nsi')('relatedDfs');
    page = await global.__BROWSER__.newPage();
    await page.setViewport({ width: 1200, height: 800 });
    const url = global._E2E_.makeUrl(
      `/vis?df[ds]=ds-demo-release&df[id]=DF_ALL_TOURISM_TRIPS&df[ag]=OECD.CFE&df[vs]=5.0&hc[Activity]=Tourism&pd=%2C&dq=.......TDS..A&ly[cl]=TIME_PERIOD&ly[rs]=REPORTING_COUNTRY&ly[rw]=MEASURE%2CVISITOR_TYPE%2CACCOMMODATION_TYPE&to[TIME_PERIOD]=false&lo=5&lom=LASTNPERIODS&vw=ov`,
    );
    await page.goto(url);
    done();
  });
  it('load the overview page', async done => {
    await changeVibe('sfs')('oecd');
    await page.$('div[id="id_overview_component"]');
    done();
  });
  it('should display related dataflows', async done => {
    const relatedDFs = await page.waitForSelector(
      testidSelector('complementaryData'),
    );
    const liCount = await relatedDFs.$$eval('ul > li', li => li.length);
    expect(liCount).toEqual(2);
    done();
  });
});
