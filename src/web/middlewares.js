import * as R from 'ramda';
import { push, replace, replaceStraight } from '@lagunovsky/redux-react-router';
import { crypto } from 'jsrsasign';
import {
  DOWNLOAD_FILE_SUCCESS,
  SHARE_SUCCESS,
  CHANGE_LAYOUT,
  CHANGE_VIEWER,
} from './ducks/vis';
import {
  REQUEST_SEARCH_DATAFILE,
  REQUEST_VIS_DATAFILE,
  HANDLE_STRUCTURE,
} from './ducks/sdmx';
import { CHANGE_CONSTRAINTS, CHANGE_TERM } from './ducks/search';
import { fromStateToSearch } from './utils/router';
import { getLocationState, getPathname } from './selectors/router';
import { getRawDataRequestArgs } from './selectors/sdmx';
import { getVisDataDimensions, getVisDataflow } from './selectors';
import { sendEvent } from './utils/analytics';
import { compactLayout } from './lib/layout';
import { CHANGE_LOCALE, USER_SIGNED_IN, USER_SIGNED_OUT } from './ducks/app';
import { UPDATE_CHART_CONFIG } from './ducks/chart-configs';
import { getFilename } from './lib/sdmx';

const isDev = process.env.NODE_ENV === 'development';

// you can push and replace in the history
// but "pushHistory" will be taken first compared to "replaceHistory"
const historyActions = [
  ['pushHistory', push],
  ['replaceHistory', replace],
  ['replaceStraigthHistory', replaceStraight],
];
export const historyMiddleware = store => next => action => {
  const historyAction = R.find(
    R.pipe(R.head, R.flip(R.prop)(action), R.isNil, R.not),
    historyActions,
  );
  if (R.isNil(historyAction)) return next(action);
  const [key, method] = historyAction;
  const actionHistory = R.propOr({}, key, action);
  const prevState = getLocationState(store.getState());
  const pathname = R.defaultTo(
    getPathname(store.getState()),
    actionHistory.pathname,
  );
  const state = R.ifElse(
    R.isNil,
    // reset if no payload with invariant id present (see app duck)
    R.always(R.pick(['locale', 'hasAccessibility', 'tenant'], prevState)),
    R.mergeRight(prevState),
  )(actionHistory.payload);

  // fromStateToSearch is an evolution centered on url
  // the following evolution relies on selectors (more generally on things in state)
  const nextState = R.evolve({
    layout: compactLayout(getVisDataDimensions()(store.getState())),
  })(state);

  store.dispatch(
    method(
      {
        pathname,
        search: fromStateToSearch(nextState),
      },
      state,
    ),
  );

  // eslint-disable-next-line no-console
  if (isDev) console.info(`[MDL] history ${action.type} -> ${pathname}`);

  return next(action);
};

const analyticsActions = new Set([
  REQUEST_SEARCH_DATAFILE,
  REQUEST_VIS_DATAFILE,
  DOWNLOAD_FILE_SUCCESS,
  SHARE_SUCCESS,
  HANDLE_STRUCTURE,
  CHANGE_LOCALE,
  CHANGE_TERM,
  CHANGE_LAYOUT,
  CHANGE_VIEWER,
  CHANGE_CONSTRAINTS,
  USER_SIGNED_IN,
  USER_SIGNED_OUT,
  UPDATE_CHART_CONFIG,
]);

export const formatConstraints = pushHistory => {
  //function for browse_by event
  const constraints = R.pipe(
    R.path(['payload', 'constraints']),
    R.defaultTo({}),
  )(pushHistory);
  const formattedConstraints = R.map(constraint => {
    //get object without knowing the key after "constraint"
    const { facetId, constraintId } = constraint;
    return facetId + '=' + constraintId;
  }, constraints);
  const concatConstraints = R.join(' > ', R.values(formattedConstraints)); //get values then join them
  return concatConstraints;
};

export const getUserId = action => {
  const user = R.prop('user', action);
  return R.isNil(user) || R.isNil(user.email)
    ? undefined
    : crypto.Util.hashString(user.email, 'SHA256');
};

export const analyticsMiddleware = ({ getState }) => next => action => {
  if (!analyticsActions.has(action.type)) return next(action);
  const future = next(action); // need to be after for HANDLE_STRUCTURE

  // eslint-disable-next-line no-console
  if (isDev) console.info(`[MDL] analytics ${action.type}`);

  const { pushHistory = {} } = action;

  let dataLayer;
  const dataflow = getVisDataflow(getState());

  switch (action.type) {
    case REQUEST_SEARCH_DATAFILE: //download unfiltered CSV whithout clicking on the result
      dataLayer = {
        event: 'file_download',
        file_extension: 'csv',
        file_name: getFilename({
          isFull: true,
          identifiers: {
            ...action.payload.dataflow,
            code: R.path(['payload', 'dataflow', 'dataflowId'], action),
          },
        }),
      };
      break;
    case REQUEST_VIS_DATAFILE: //download filtered or unfiltered CSV
      dataLayer = {
        event: 'file_download',
        file_extension: 'csv',
        file_name: getFilename({
          ...getRawDataRequestArgs(getState()),
          isFull: action.payload.isDownloadAllData,
        }),
      };
      break;
    case DOWNLOAD_FILE_SUCCESS: //download Table (excel) or Chart (PNG)
      dataLayer = {
        event: 'file_download',
        file_extension: action.payload.id,
        file_name: action.payload.fileName,
      };
      break;
    case SHARE_SUCCESS:
      dataLayer = {
        event: 'share',
        social_network: null,
      };
      break;
    case HANDLE_STRUCTURE:
      dataLayer = {
        event: 'load_dataflow',
        dataset_code: R.prop('id', dataflow),
        dataset_name: R.prop('name', dataflow),
      };
      break;
    case USER_SIGNED_IN:
      dataLayer = {
        isGTMEvent: true,
        event: 'login',
        is_logged: true,
        user_id: getUserId(action),
      };
      break;
    case USER_SIGNED_OUT:
      dataLayer = {
        isGTMEvent: true,
        event: 'login',
        is_logged: false,
        user_id: undefined,
      };
      break;
    case CHANGE_CONSTRAINTS:
      dataLayer = {
        isGTMEvent: true,
        event: 'browse_by',
        browse_by_value: formatConstraints(pushHistory),
      };
      break;
    case CHANGE_TERM:
      dataLayer = {
        isGTMEvent: true,
        event: 'view_search_results',
        search_term: pushHistory.payload.term,
      };
      break;
    case CHANGE_LOCALE:
      dataLayer = {
        isGTMEvent: true,
        event: 'select_language',
        language_method: 'dropdown',
        language_selected: pushHistory.payload.locale,
      };
      break;
    case CHANGE_LAYOUT:
    case UPDATE_CHART_CONFIG:
      if (R.isEmpty(pushHistory)) return;
      dataLayer = {
        isGTMEvent: true,
        event: 'change_layout',
        dataset_code: R.prop('id', dataflow),
        dataset_name: R.prop('name', dataflow),
      };
      break;
    case CHANGE_VIEWER:
      dataLayer = {
        isGTMEvent: true,
        event: 'change_viewer',
        chart_type: pushHistory.payload.viewer,
        dataset_code: R.prop('id', dataflow),
        dataset_name: R.prop('name', dataflow),
      };
      break;
    default:
      // eslint-disable-next-line no-console
      if (isDev) console.log(`[MDL] analytics unknown type ${action.type}`);
      return future;
  }
  sendEvent({ dataLayer });
  return future;
};
