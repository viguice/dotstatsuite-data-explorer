const e2eConfig = require('./jest.config.e2e');

module.exports = {
  ...e2eConfig,
  testMatch: ['**/src/**/tests/**/*.e2e.perf.js'],
  testTimeout: 3000000,
};
